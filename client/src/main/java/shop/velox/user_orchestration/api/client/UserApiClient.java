package shop.velox.user_orchestration.api.client;

import java.io.Serializable;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import lombok.extern.slf4j.Slf4j;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.commons.rest.response.RestResponsePage;
import static shop.velox.commons.utils.PaginationUtils.addPageableInformation;
import static shop.velox.commons.utils.QueryParamsUtils.addQueryParams;
import shop.velox.user.enumerations.UserStatus;
import shop.velox.user_orchestration.api.dto.CreateUserZitadelDto;
import shop.velox.user_orchestration.api.dto.UserDto;
import shop.velox.user_orchestration.api.dto.ZitadelUserDto;

@Slf4j
public class UserApiClient {

  private final RestTemplate restTemplate;

  private final String baseUrl;

  public UserApiClient(RestTemplate restTemplate, String hostUrl) {
    this.restTemplate = restTemplate;
    this.baseUrl = hostUrl + "/user-orchestration/v1/users";
  }

  protected static URI getUsersUri(String baseUrl, GetUsersFilters filters, Pageable pageable) {
    UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(baseUrl);

    Optional.ofNullable(filters)
        .map(GetUsersFilters::toMultiValueMap)
        .ifPresent(queryParams -> addQueryParams(uriComponentsBuilder, queryParams));
    addPageableInformation(uriComponentsBuilder, pageable);

    URI uri = uriComponentsBuilder
        .build(true)
        .toUri();
    return uri;
  }

  public ResponseEntity<UserDto> getUser(String userCode) {

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(userCode)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("getUser with URI: {}", uri);

    return restTemplate.exchange(uri, GET, null/*httpEntity*/, UserDto.class);
  }

  public ResponseEntity<UserDto> getCurrentUser() {

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("me")
        .build()
        .toUri();

    log.debug("getCurrentUser with URI: {}", uri);

    return restTemplate.exchange(uri, GET, null/*httpEntity*/, UserDto.class);
  }

  public ResponseEntity<ZitadelUserDto> getOwnZitadelIdentity() {

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("me", "identity")
        .build()
        .toUri();

    log.debug("getOwnZitadelIdentity with URI: {}", uri);

    return restTemplate.exchange(uri, GET, null/*httpEntity*/, ZitadelUserDto.class);
  }

  public ResponseEntity<? extends Page<UserDto>> getUsers(GetUsersFilters filters,
      @Nullable Pageable pageable) {

    URI uri = getUsersUri(baseUrl, filters, pageable);

    log.debug("getUsers with URI: {}", uri);

    ParameterizedTypeReference<RestResponsePage<UserDto>> responseType = new ParameterizedTypeReference<>() {
    };
    return restTemplate.exchange(uri, GET, null/*httpEntity*/, responseType);
  }

  public ResponseEntity<UserDto> createZitadelIdentity(String userCode,
      CreateUserZitadelDto createUserZitadelDto) {
    HttpHeaders headers = new HttpHeaders();
    HttpEntity<CreateUserZitadelDto> httpEntity = new HttpEntity<>(createUserZitadelDto, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(userCode, "identity")
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("createZitadelIdentity with URI: {}", uri);

    return restTemplate.exchange(uri, POST, httpEntity, UserDto.class);

  }

  @Value
  @Builder
  @Jacksonized
  @Validated
  public static class GetUsersFilters {

    @Nullable
    List<String> codes;

    @Nullable
    String email;

    @Nullable
    String firstName;

    @Nullable
    String lastName;

    @Nullable
    String unitCode;

    List<UserStatus> statuses;

    public MultiValueMap<String, String> toMultiValueMap() {
      MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

      if (isNotEmpty(codes)) {
        map.put("code", codes);
      }
      if (isNotBlank(email)) {
        map.add("email", email);
      }
      if (isNotBlank(firstName)) {
        map.add("firstName", firstName);
      }
      if (isNotBlank(lastName)) {
        map.add("lastName", lastName);
      }
      if (isNotBlank(unitCode)) {
        map.add("unitCode", unitCode);
      }
      if (isNotEmpty(statuses)) {
        List<String> statusList = statuses.stream()
            .map(Enum::name)
            .toList();
        map.put("status", statusList);
      }
      return map;
    }
  }

}
