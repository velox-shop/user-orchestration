package shop.velox.user_orchestration.api.client;

import java.io.Serializable;
import java.net.URI;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import lombok.Builder;
import lombok.Value;
import lombok.extern.jackson.Jacksonized;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import static org.springframework.http.HttpMethod.GET;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.commons.rest.response.RestResponsePage;
import static shop.velox.commons.utils.PaginationUtils.addPageableInformation;
import static shop.velox.commons.utils.QueryParamsUtils.addQueryParams;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.enumerations.UnitStatus;

@Slf4j
public class UnitApiClient {

  private final RestTemplate restTemplate;

  private final String baseUrl;

  public UnitApiClient(RestTemplate restTemplate, String hostUrl) {
    this.restTemplate = restTemplate;
    this.baseUrl = hostUrl + "/user-orchestration/v1/units";
  }

  protected static URI getUnitsUri(String baseUrl, GetUnitsFilters filters, Pageable pageable) {
    UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(baseUrl);

    Optional.ofNullable(filters)
        .map(GetUnitsFilters::toMultiValueMap)
        .ifPresent(queryParams -> addQueryParams(uriComponentsBuilder, queryParams));

    addPageableInformation(uriComponentsBuilder, pageable);

    return uriComponentsBuilder
        .build(true)
        .toUri();
  }

  public ResponseEntity<? extends Page<UnitDto>> getUnits(GetUnitsFilters filters,
      Pageable pageable) {
    URI uri = getUnitsUri(baseUrl, filters, pageable);

    log.debug("getUnits with URI: {}", uri);

    ParameterizedTypeReference<RestResponsePage<UnitDto>> responseType = new ParameterizedTypeReference<>() {
    };
    return restTemplate.exchange(uri, GET, null/*httpEntity*/, responseType);
  }

  public ResponseEntity<UnitDto> getUnit(String unitCode) {

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment(unitCode)
        .buildAndExpand(uriPathParams)
        .toUri();

    log.debug("getUnit with URI: {}", uri);

    return restTemplate.exchange(uri, GET, null/*httpEntity*/, UnitDto.class);
  }

  public ResponseEntity<UnitDto> getZitadelUnit() {

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("me")
        .build()
        .toUri();

    log.debug("getZitadelUnit with URI: {}", uri);

    return restTemplate.exchange(uri, GET, null/*httpEntity*/, UnitDto.class);
  }

  @Value
  @Builder
  @Jacksonized
  @Validated
  public static class GetUnitsFilters {

    String code;

    String name;

    List<UnitStatus> statuses;

    String userCode;

    @Nullable
    String searchText;

    public MultiValueMap<String, String> toMultiValueMap() {
      MultiValueMap<String, String> map = new LinkedMultiValueMap<>();

      Optional.ofNullable(code)
          .filter(StringUtils::isNotBlank)
          .ifPresent(v -> map.add("code", v));

      Optional.ofNullable(name)
          .filter(StringUtils::isNotBlank)
          .ifPresent(v -> map.add("name", v));

      Optional.ofNullable(statuses)
          .filter(CollectionUtils::isNotEmpty)
          .ifPresent(v -> v.forEach(s -> map.add("status", s.name())));

      Optional.ofNullable(userCode)
          .filter(StringUtils::isNotBlank)
          .ifPresent(v -> map.add("userCode", v));

      Optional.ofNullable(searchText)
          .filter(StringUtils::isNotBlank)
          .ifPresent(v -> map.add("searchText", v));

      return map;
    }

  }

}
