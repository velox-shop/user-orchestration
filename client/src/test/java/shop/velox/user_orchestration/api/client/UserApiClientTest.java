package shop.velox.user_orchestration.api.client;

import java.net.URI;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import shop.velox.user.enumerations.UserStatus;
import shop.velox.user_orchestration.api.client.UserApiClient.GetUsersFilters;
import static shop.velox.user_orchestration.api.client.UserApiClient.getUsersUri;

class UserApiClientTest {

  @Test
  @DisplayName("Verify that umlaut characters are encoded correctly")
  void getUsersUriUmlautTest() {

    GetUsersFilters filters = GetUsersFilters.builder()
        .firstName("myNäme")
        .build();
    var uri = getUsersUri("http://localhost/user/v1/users", filters, null);

    Assertions.assertThat(uri).isNotNull()
        .extracting(URI::toString)
        .isEqualTo("http://localhost/user/v1/users?firstName=myN%C3%A4me");
  }

  @Test
  @DisplayName("Verify that a plus character is encoded correctly")
  void getUsersUriPlusTest() {

    GetUsersFilters filters = GetUsersFilters.builder()
        .email("email+1@example.com")
        .build();
    var uri = getUsersUri("http://localhost/user/v1/users", filters, null);

    Assertions.assertThat(uri).isNotNull()
        .extracting(URI::toString)
        .isEqualTo("http://localhost/user/v1/users?email=email%2B1%40example.com");
  }

  @Test
  @DisplayName("Verify that multiple statuses are correctly put into the URI")
  void getUnitsMultipleStatusesTest() {

    GetUsersFilters filters = GetUsersFilters.builder()
        .statuses(List.of(UserStatus.ACTIVE, UserStatus.INACTIVE))
        .build();
    var uri = getUsersUri("http://localhost/user/v1/users", filters, null);

    Assertions.assertThat(uri).isNotNull()
        .extracting(URI::toString)
        .isEqualTo("http://localhost/user/v1/users?status=ACTIVE&status=INACTIVE");
  }

  @Test
  @DisplayName("Verify that Pagination is correctly put into the URI")
  void getUnitsPaginationTest() {

    Pageable pageable = PageRequest.ofSize(10).withPage(1).withSort(Direction.DESC, "name");
    var uri = getUsersUri("http://localhost/user/v1/users", null, pageable);

    Assertions.assertThat(uri).isNotNull()
        .extracting(URI::toString)
        .isEqualTo("http://localhost/user/v1/users?page=1&size=10&sort=name,desc");
  }
}
