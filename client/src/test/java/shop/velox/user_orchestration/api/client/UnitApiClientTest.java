package shop.velox.user_orchestration.api.client;

import java.net.URI;
import java.util.List;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import shop.velox.user.enumerations.UnitStatus;
import shop.velox.user_orchestration.api.client.UnitApiClient.GetUnitsFilters;
import static shop.velox.user_orchestration.api.client.UnitApiClient.getUnitsUri;

class UnitApiClientTest {

  @Test
  @DisplayName("Verify that umlaut characters are encoded correctly")
  void getUnitsUriUmlautTest() {

    GetUnitsFilters filters = GetUnitsFilters.builder()
        .name("myNäme")
        .build();
    var uri = getUnitsUri("http://localhost/user/v1/units", filters, null);

    Assertions.assertThat(uri).isNotNull()
        .extracting(URI::toString)
        .isEqualTo("http://localhost/user/v1/units?name=myN%C3%A4me");
  }

  @Test
  @DisplayName("Verify that a plus character is encoded correctly")
  void getUnitsUriPlusTest() {

    GetUnitsFilters filters = GetUnitsFilters.builder()
        .code("code+foo")
        .build();
    var uri = getUnitsUri("http://localhost/user-orchestration/v1/units", filters, null);

    Assertions.assertThat(uri).isNotNull()
        .extracting(URI::toString)
        .isEqualTo("http://localhost/user-orchestration/v1/units?code=code%2Bfoo");
  }

  @Test
  @DisplayName("Verify that multiple statuses are correctly put into the URI")
  void getUnitsMultipleStatusesTest() {

    GetUnitsFilters filters = GetUnitsFilters.builder()
        .statuses(List.of(UnitStatus.ACTIVE, UnitStatus.INACTIVE))
        .build();
    var uri = getUnitsUri("http://localhost/user/v1/units", filters, null);

    Assertions.assertThat(uri).isNotNull()
        .extracting(URI::toString)
        .asString()
        .contains("status=ACTIVE&status=INACTIVE");
  }

  @Test
  @DisplayName("Verify that Pagination is correctly put into the URI")
  void getUnitsPaginationTest() {

    Pageable pageable = PageRequest.ofSize(10).withPage(1).withSort(Direction.DESC, "name");
    var uri = getUnitsUri("http://localhost/user/v1/units", null, pageable);

    Assertions.assertThat(uri).isNotNull()
        .extracting(URI::toString)
        .isEqualTo("http://localhost/user/v1/units?page=1&size=10&sort=name,desc");
  }
}
