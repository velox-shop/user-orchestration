#!/bin/sh
echo "Waiting for all services to become healthy..."

# Check if docker-compose is up and running
if ! docker-compose ps &> /dev/null
then
    echo "docker-compose is not running. Please start docker-compose and try again."
    exit 1
fi

# Get the names of all services from the docker-compose file
services=$(docker-compose config --services)

# Function to check if a service is healthy
check_service_health() {
    service=$1
    service_id=$(docker-compose ps -q "$service")

    # Check if the service is up
    if [ -z "$service_id" ]
    then
        echo "$service is not running."
        return 1
    fi

    health_status=$(docker inspect --format "{{.State.Health.Status}}" "$service_id")

    if [ "$health_status" = "healthy" ]; then
        return 0
    else
        return 1
    fi
}

# Loop over each service
for service in $services
do
    echo "Waiting for $service to become healthy..."
    start_time=$(date +%s)
    while ! check_service_health $service
    do
        # Check if 60 seconds have passed
        current_time=$(date +%s)
        elapsed_time=$(($current_time-$start_time))
        if [ $elapsed_time -ge 60 ]; then
            echo "Stopped waiting for $service after 60 seconds."
            break
        fi

        # Wait for 5 seconds before checking the health status again
        sleep 5
    done
    if check_service_health $service; then
        echo "$service is healthy."
    fi
done

echo "Health check completed."
