#!/bin/sh

mkdir logs

# Get the names of all services from the docker-compose file
services=$(docker-compose config --services)

# Loop over each service
for service in $services
do
    # Create the log command for each service and run it in the background
    nohup docker-compose logs --follow $service > logs/${service}.txt &
done

echo "Logging started for services: $services"
