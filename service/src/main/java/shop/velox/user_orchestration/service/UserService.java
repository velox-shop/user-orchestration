package shop.velox.user_orchestration.service;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import shop.velox.user.api.dto.user.CreateUserDto;
import shop.velox.user.api.dto.user.UpdateUserDto;
import shop.velox.user.enumerations.UserStatus;
import shop.velox.user_orchestration.api.dto.CreateUserZitadelDto;
import shop.velox.user_orchestration.api.dto.UserDto;

public interface UserService {

  UserDto createUser(CreateUserDto user);

  UserDto getUser(String userCode);

  UserDto getCurrentUser();

  UserDto updateUser(String userCode, UpdateUserDto user);

  Page<UserDto> getUsers(String firstName, String lastName, String email, String unitCode,
      List<String> userCodes, List<UserStatus> statuses, Pageable pageable);

  UserDto upsertUserIdentity(String userCode, CreateUserZitadelDto createUserZitadelDto);

}
