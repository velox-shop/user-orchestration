package shop.velox.user_orchestration.service.impl;

import shop.velox.user_orchestration.service.UnitService;
import shop.velox.user_orchestration.service.UserIdentityService;
import io.micrometer.common.util.StringUtils;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.user.api.controller.client.UnitApiClient;
import shop.velox.user.api.controller.client.UnitApiClient.GetUnitsFilters;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.enumerations.UnitStatus;
import shop.velox.user_orchestration.api.dto.ZitadelUserDto;

@Service
@RequiredArgsConstructor
@Slf4j
public class UnitServiceImpl implements UnitService {

  private final UnitApiClient unitApiClient;

  private final UserIdentityService userIdentityService;

  @Override
  public Page<UnitDto> getUnits(@Nullable String name, @Nullable String code,
      List<UnitStatus> statuses, String userCode, @Nullable String searchText, Pageable pageable) {
    ResponseEntity<? extends Page<UnitDto>> responseEntity;
    try {
      List<String> searchTexts = getSearchTexts(searchText);
      GetUnitsFilters filters = GetUnitsFilters.builder()
          .name(name)
          .code(code)
          .statuses(statuses)
          .userCode(userCode)
          .searchTexts(searchTexts)
          .build();
      responseEntity = unitApiClient.getUnits(filters, pageable);
    } catch (HttpClientErrorException.NotFound | HttpClientErrorException.Forbidden e) {
      throw new ResponseStatusException(e.getStatusCode(), e.getMessage());
    } catch (HttpStatusCodeException e) {
      log.error("Error while getting units with name: {}", name, e);
      throw e;
    }
    return responseEntity.getBody();
  }

  protected static List<String> getSearchTexts(String searchText) {
    // filter out blank strings
    return Optional.ofNullable(searchText)
        .map(text -> text.split(" "))
        .map(Arrays::asList)
        .map(strings -> strings.stream()
            .filter(StringUtils::isNotBlank)
            .toList())
        .orElse(null);
  }

  @Override
  public UnitDto getUnit(String unitCode) {
    ResponseEntity<UnitDto> responseEntity;
    try {
      responseEntity = getUnitDtoResponseEntity(unitCode);
    } catch (HttpClientErrorException.NotFound | HttpClientErrorException.Forbidden e) {
      throw new ResponseStatusException(e.getStatusCode(), e.getMessage());
    } catch (HttpStatusCodeException e) {
      log.error("Error while getting unit with code: {}", unitCode, e);
      throw e;
    }
    return responseEntity.getBody();
  }

  @Override
  public ResponseEntity<UnitDto> getUnitDtoResponseEntity(String unitCode) {
    return unitApiClient.getUnit(unitCode);
  }

  @Override
  public Optional<UnitDto> getZitadelUnit() {
    return Optional.of(userIdentityService.getOwnUser()).map(ZitadelUserDto::getUnitExternalId)
        .map(unitExternalId -> GetUnitsFilters.builder().externalId(unitExternalId).build())
        .map(filters -> unitApiClient.getUnits(filters, Pageable.ofSize(1)))
        .map(ResponseEntity::getBody)
        .map(Page::getContent)
        .flatMap(units -> units.stream().findFirst());
  }
}
