package shop.velox.user_orchestration.service;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "velox.user")
@Data
@Validated
public class UserConfiguration {

  @NotNull
  @NotBlank
  String host;

}
