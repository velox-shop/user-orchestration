package shop.velox.user_orchestration.service;

import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.enumerations.UnitStatus;

public interface UnitService {

  Page<UnitDto> getUnits(@Nullable String name, @Nullable String code, List<UnitStatus> statuses,
      @Nullable String userCode, @Nullable String searchText, Pageable pageable);

  UnitDto getUnit(String unitCode);

  ResponseEntity<UnitDto> getUnitDtoResponseEntity(String unitCode);

  Optional<UnitDto> getZitadelUnit();
}
