package shop.velox.user_orchestration.service.impl;

import shop.velox.user_orchestration.ZitadelConfiguration;
import shop.velox.user_orchestration.converter.ZitadelUserConverter;
import shop.velox.user_orchestration.service.UserIdentityService;
import shop.velox.user_orchestration.zitadel.ZitadelOrganizationService;
import shop.velox.user_orchestration.zitadel.ZitadelUserService;
import shop.velox.user_orchestration.zitadel.dto.unit.ZitadelUnitDto;
import shop.velox.user_orchestration.zitadel.dto.user.UserContainerDto;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user_orchestration.api.dto.CreateUserZitadelDto;
import shop.velox.user_orchestration.api.dto.UserDto;
import shop.velox.user_orchestration.api.dto.ZitadelUserDto;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserIdentityServiceImpl implements UserIdentityService {

  private final ZitadelConfiguration zitadelConfiguration;

  private final ZitadelOrganizationService zitadelOrganizationService;

  private final ZitadelUserService zitadelUserService;

  private final ZitadelUserConverter zitadelUserConverter;

  @Override
  public UserDto upsertUserIdentity(CreateUserZitadelDto createUserZitadelDto,
      UnitDto unitDto, shop.velox.user.api.dto.user.UserDto serviceUserDto) {
    String userCode = serviceUserDto.getCode();
    List<String> passedRoles = createUserZitadelDto.getRoles();
    List<String> defaultRoles = zitadelConfiguration.getDefaultUserRoles();
    List<String> roles = CollectionUtils.isEmpty(passedRoles) ? defaultRoles : passedRoles;
    log.info("Upserting user identity for userCode: {} with roles: {}", userCode, roles);

    ZitadelUnitDto updatedUnitDto = zitadelOrganizationService.upsertOrganization(unitDto);

    return zitadelUserService.upsertHumanUser(serviceUserDto, updatedUnitDto, roles);
  }

  @Override
  public ZitadelUserDto getOwnUser() {
    log.info("Getting Zitadel Info for logged in user");

    UserContainerDto ownUser = zitadelUserService.getOwnUser();
    return zitadelUserConverter.convert(ownUser);
  }
}
