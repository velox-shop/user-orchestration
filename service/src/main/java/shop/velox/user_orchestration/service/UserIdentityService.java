package shop.velox.user_orchestration.service;

import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user_orchestration.api.dto.CreateUserZitadelDto;
import shop.velox.user_orchestration.api.dto.UserDto;
import shop.velox.user_orchestration.api.dto.ZitadelUserDto;

public interface UserIdentityService {

  UserDto upsertUserIdentity(CreateUserZitadelDto createUserZitadelDto,
      UnitDto unitDto, shop.velox.user.api.dto.user.UserDto serviceUserDto);

  ZitadelUserDto getOwnUser();
}
