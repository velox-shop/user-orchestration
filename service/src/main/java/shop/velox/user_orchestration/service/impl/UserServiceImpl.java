package shop.velox.user_orchestration.service.impl;

import java.util.Collection;
import static java.util.Collections.emptyMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.collections4.CollectionUtils.isNotEmpty;
import static org.apache.commons.collections4.CollectionUtils.subtract;
import static org.apache.commons.collections4.ListUtils.emptyIfNull;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.commons.lang3.StringUtils.isNotBlank;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import static org.springframework.http.HttpStatus.UNPROCESSABLE_ENTITY;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.user.api.controller.client.UnitApiClient;
import shop.velox.user.api.controller.client.UserApiClient;
import shop.velox.user.api.controller.client.UserApiClient.GetUsersFilters;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.api.dto.user.CreateUserDto;
import shop.velox.user.api.dto.user.UpdateUserDto;
import static shop.velox.user.enumerations.UnitStatus.INACTIVE;
import shop.velox.user.enumerations.UserStatus;
import shop.velox.user_orchestration.ZitadelConfiguration;
import shop.velox.user_orchestration.api.dto.CreateUserZitadelDto;
import shop.velox.user_orchestration.api.dto.UserDto;
import shop.velox.user_orchestration.converter.JsonToStringConverter;
import shop.velox.user_orchestration.converter.UserConverter;
import shop.velox.user_orchestration.service.UnitService;
import shop.velox.user_orchestration.service.UserIdentityService;
import shop.velox.user_orchestration.service.UserService;
import shop.velox.user_orchestration.zitadel.ZitadelUserService;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

  private final UserApiClient userApiClient;

  private final UserConverter userConverter;

  private final UserIdentityService userIdentityService;

  private final ZitadelUserService zitadelUserService;

  private final UnitService unitService;

  private final UnitApiClient unitApiClient;

  private final ZitadelConfiguration zitadelConfiguration;

  private final JsonToStringConverter jsonToStringConverter;


  @Override
  public UserDto createUser(CreateUserDto user) {
    try {
      var serviceUser = userApiClient.createUser(user).getBody();
      return userConverter.convert(serviceUser);
    } catch (HttpClientErrorException e) {
      log.error("Error in createUser: {}", e.getMessage());
      throw new ResponseStatusException(e.getStatusCode(), e.getMessage());
    }
  }

  @Override
  public UserDto getUser(String userCode) {
    try {
      var serviceUser = userApiClient.getUser(userCode).getBody();
      return userConverter.convert(serviceUser);
    } catch (HttpClientErrorException e) {
      log.error("Error in getUser: {}", e.getMessage());
      throw new ResponseStatusException(e.getStatusCode(), e.getMessage());
    }
  }

  @Override
  public UserDto getCurrentUser() {
    try {
      var serviceUser = userApiClient.getCurrentUser().getBody();
      return userConverter.convert(serviceUser);
    } catch (HttpClientErrorException e) {
      log.error("Error in getCurrentUser: {}", e.getMessage());
      throw new ResponseStatusException(e.getStatusCode(), e.getMessage());
    }
  }


  @Override
  public UserDto updateUser(String userCode, UpdateUserDto user) {
    try {
      var serviceUser = userApiClient.updateUser(userCode, user).getBody();
      return userConverter.convert(serviceUser);
    } catch (HttpClientErrorException e) {
      log.error("Error in updateUser: {}", e.getMessage());
      throw new ResponseStatusException(e.getStatusCode(), e.getMessage());
    }
  }

  @Override
  public Page<UserDto> getUsers(String firstName, String lastName, String email, String unitCode,
      List<String> userCodes, List<UserStatus> statuses, Pageable pageable) {
    try {
      GetUsersFilters filters = GetUsersFilters.builder()
          .firstName(firstName)
          .lastName(lastName)
          .email(email)
          .unitCode(unitCode)
          .codes(userCodes)
          .statuses(statuses)
          .build();
      var serviceUsersPage = userApiClient.getUsers(filters, pageable)
          .getBody();
      if (isNotBlank(unitCode)) {
        var unit = unitApiClient.getUnit(unitCode).getBody();
        if (unit != null) {
          String unitExternalId = unit.getExternalId();
          Map<String, List<String>> user2RolesMap =
              isNotBlank(unitExternalId) ? getUsersRoles(unit) : emptyMap();
          return serviceUsersPage.map(user -> userConverter.convert(user, unit, user2RolesMap));

        }
      }
      return serviceUsersPage.map(userConverter::convert);
    } catch (HttpClientErrorException e) {
      log.error("Error in getUsers: {}", e.getMessage());
      throw new ResponseStatusException(e.getStatusCode(), e.getMessage());
    }
  }

  private Map<String, List<String>> getUsersRoles(UnitDto unit) {
    try {
      return zitadelUserService.getUsersRoles(unit.getExternalId());
    } catch (RestClientException e) {
      log.error("Error in getUsersRoles for unit: {}: {}", unit, e.getMessage());
      return emptyMap();
    }
  }

  @Override
  public UserDto upsertUserIdentity(String userCode, CreateUserZitadelDto createUserZitadelDto) {

    log.info("Upserting User Identity for User: {} with {}", userCode,
        jsonToStringConverter.toString(createUserZitadelDto));

    var serviceUserDto = userApiClient.getUser(userCode).getBody();
    if (isBlank(serviceUserDto.getEmail())) {
      log.warn("Cannot upsert User Identity because User: {} has no email address", userCode);
      throw new ResponseStatusException(UNPROCESSABLE_ENTITY,
          "User has no email address");
    }

    validateRoles(userCode, emptyIfNull(createUserZitadelDto.getRoles()));

    if (!emptyIfNull(serviceUserDto.getUnitCodes()).contains(createUserZitadelDto.getUnitCode())) {
      log.warn("Cannot upsert User Identity because User: {} is not in Unit: {}", userCode,
          createUserZitadelDto.getUnitCode());
      throw new ResponseStatusException(UNPROCESSABLE_ENTITY, "User is not in Unit");
    }

    String userDtoExternalId = serviceUserDto.getExternalId();
    if (isNotBlank(userDtoExternalId)) {
      log.warn("Cannot upsert User Identity because User: {} already has Zitadel User: {}",
          userCode, userDtoExternalId);
      throw new ResponseStatusException(UNPROCESSABLE_ENTITY, "User is already a Zitadel User");
    }

    var unitCode = createUserZitadelDto.getUnitCode();
    UnitDto unitDto = getUnit(serviceUserDto, unitCode);
    if (INACTIVE == unitDto.getStatus()) {
      log.warn("Cannot upsert User Identity for {} because Unit: {} is inactive", userCode,
          unitCode);
      throw new ResponseStatusException(UNPROCESSABLE_ENTITY, "Unit is inactive");
    }

    return userIdentityService.upsertUserIdentity(createUserZitadelDto, unitDto, serviceUserDto);

  }

  protected void validateRoles(String userCode, @NonNull List<String> roles) {
    if (isEmpty(roles)) {
      log.debug("No roles provided for user: {}. Defaults will be applied", userCode);
      return;
    }

    if (roles.size() != new HashSet<>(roles).size()) {
      log.warn(
          "Cannot upsert User Identity for {} with roles: {} because roles contain duplicates",
          userCode, roles);
      throw new ResponseStatusException(UNPROCESSABLE_ENTITY, "Roles contain duplicates");
    }

    List<String> projectRoles = zitadelConfiguration.getProjectRoles();
    log.debug("Project Roles: {}", projectRoles);
    Collection<String> extra = subtract(roles, projectRoles);
    if (isNotEmpty(extra)) {
      log.warn(
          "Cannot upsert User Identity for {} with roles: {} because they contain unknown roles: {}",
          userCode, roles, extra);
      throw new ResponseStatusException(UNPROCESSABLE_ENTITY, "Roles contain unknown roles");
    }
  }

  private UnitDto getUnit(shop.velox.user.api.dto.user.UserDto user, String unitCode) {
    try {
      return unitService.getUnitDtoResponseEntity(unitCode).getBody();
    } catch (HttpClientErrorException.Forbidden e) {
      throw new ResponseStatusException(e.getStatusCode(), e.getMessage());
    } catch (HttpClientErrorException.NotFound e) {
      log.warn("Unit {} not found for user: {}", unitCode, user.getCode());
      throw new ResponseStatusException(UNPROCESSABLE_ENTITY,
          "Unit does not exist");
    } catch (Exception e) {
      log.error("Error {} in getUnit: {} for user: {}", e.getMessage(), unitCode, user.getCode());
      throw new ResponseStatusException(UNPROCESSABLE_ENTITY,
          "Cannot get unit for user");
    }
  }

}
