package shop.velox.user_orchestration;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import shop.velox.commons.rest.filters.RestTemplateHeaderModifierInterceptor;
import shop.velox.user.api.controller.client.UnitApiClient;
import shop.velox.user.api.controller.client.UserApiClient;
import shop.velox.user_orchestration.converter.JsonToStringConverter;
import shop.velox.user_orchestration.service.UserConfiguration;
import shop.velox.user_orchestration.zitadel.ZitadelAuthApiClient;
import shop.velox.user_orchestration.zitadel.ZitadelManagementApiClient;


@Configuration
@RequiredArgsConstructor
public class RestTemplateConfig {


  private final RestTemplateHeaderModifierInterceptor restTemplateHeaderModifierInterceptor;

  @Bean
  public RestTemplate restTemplate(RestTemplateBuilder builder) {
    return builder
        .requestFactory(HttpComponentsClientHttpRequestFactory.class)
        .additionalInterceptors(restTemplateHeaderModifierInterceptor)
        .build();
  }


  @Bean
  public UserApiClient userApiClient(RestTemplate restTemplate,
      UserConfiguration userConfiguration) {
    return new UserApiClient(restTemplate, userConfiguration.getHost());
  }

  @Bean
  public UnitApiClient unitApiClient(RestTemplate restTemplate,
      UserConfiguration userConfiguration) {
    return new UnitApiClient(restTemplate, userConfiguration.getHost());
  }

  @Bean
  public ZitadelManagementApiClient zitadelManagementApiClient(RestTemplate restTemplate,
      JsonToStringConverter jsonToStringConverter,
      @Value("${velox.zitadel.host}") String hostUrl) {
    return new ZitadelManagementApiClient(restTemplate, jsonToStringConverter, hostUrl);
  }

  @Bean
  public ZitadelAuthApiClient zitadelAuthApiClient(RestTemplate restTemplate,
      @Value("${velox.zitadel.host}") String hostUrl) {
    return new ZitadelAuthApiClient(restTemplate, hostUrl);
  }

}
