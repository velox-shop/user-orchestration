package shop.velox.user_orchestration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(
    scanBasePackages = {"shop.velox.user_orchestration", "shop.velox.commons"},
    exclude = {SecurityAutoConfiguration.class}
)
public class UserOrchestrationApplication {

  public static void main(String[] args) {
    SpringApplication.run(UserOrchestrationApplication.class, args);
  }

}
