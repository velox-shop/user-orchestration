package shop.velox.user_orchestration.zitadel;

import static org.springframework.http.HttpMethod.GET;

import shop.velox.user_orchestration.zitadel.dto.user.UserContainerDto;
import jakarta.annotation.PostConstruct;
import java.net.URI;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@RequiredArgsConstructor
@Slf4j
public class ZitadelAuthApiClient {

  private final RestTemplate restTemplate;

  private final String hostUrl;
  private String baseUrl;

  @PostConstruct
  void init() {
    baseUrl = hostUrl + "/auth/v1";
    log.info("ZitadelAuthApiClient initialized with baseUrl: {}", baseUrl);
  }

  public ResponseEntity<UserContainerDto> getOwnUser() {
    // https://zitadel.com/docs/apis/resources/auth/auth-service-get-my-user

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("users", "me")
        .build()
        .toUri();

    log.info("getOwnUser with URI: {}", uri);
    return restTemplate.exchange(uri, GET, null, UserContainerDto.class);
  }

}
