package shop.velox.user_orchestration.zitadel.dto;

import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@FieldNameConstants
public class AddUserGrantRequestDto {

  String projectId;

  String projectGrantId;

  List<String> roleKeys;

}
