package shop.velox.user_orchestration.zitadel.dto.organization;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@FieldNameConstants
public class CreateOrganizationResponseDto {

  String id;

}
