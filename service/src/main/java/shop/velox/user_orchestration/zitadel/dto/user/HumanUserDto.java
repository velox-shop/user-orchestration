package shop.velox.user_orchestration.zitadel.dto.user;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@FieldNameConstants
public class HumanUserDto {

  String id;

  String userName;

  Human human;

  Details details;

  @Value
  @Builder
  @Jacksonized
  @FieldNameConstants
  public static class Human {

    Profile profile;
  }

  @Value
  @Builder
  @Jacksonized
  @FieldNameConstants
  public static class Profile {

    String nickName;
  }

  @Value
  @Builder
  @Jacksonized
  @FieldNameConstants
  public static class Details {

    String resourceOwner;

  }

}
