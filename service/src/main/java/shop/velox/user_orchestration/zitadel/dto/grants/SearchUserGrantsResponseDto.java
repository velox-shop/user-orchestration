package shop.velox.user_orchestration.zitadel.dto.grants;

import java.time.ZonedDateTime;
import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@FieldNameConstants
public class SearchUserGrantsResponseDto {

  DetailsDTO details;
  List<ResultDTO> result;

  @Value
  @Builder
  @Jacksonized
  @FieldNameConstants
  public static class DetailsDTO {
    String totalResult;
    ZonedDateTime viewTimestamp;
  }

  @Value
  @Builder
  @Jacksonized
  @FieldNameConstants
  public static class ResultDTO {
    String id;
    ResultDetailsDTO details;
    List<String> roleKeys;
    String state;
    String userId;
    String userName;
    String firstName;
    String lastName;
    String email;
    String displayName;
    String orgId;
    String orgName;
    String orgDomain;
    String projectId;
    String projectName;
    String projectGrantId;
    String preferredLoginName;
    String userType;
    String grantedOrgId;
    String grantedOrgName;
    String grantedOrgDomain;
  }

  @Value
  @Builder
  @Jacksonized
  @FieldNameConstants
  public static class ResultDetailsDTO {
    String sequence;
    ZonedDateTime creationDate;
    ZonedDateTime changeDate;
    String resourceOwner;
  }

}
