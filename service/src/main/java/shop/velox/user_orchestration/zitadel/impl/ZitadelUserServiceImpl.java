package shop.velox.user_orchestration.zitadel.impl;

import shop.velox.user_orchestration.ZitadelConfiguration;
import shop.velox.user_orchestration.converter.UpdateUserConverter;
import shop.velox.user_orchestration.converter.UserConverter;
import shop.velox.user_orchestration.zitadel.ZitadelAuthApiClient;
import shop.velox.user_orchestration.zitadel.ZitadelManagementApiClient;
import shop.velox.user_orchestration.zitadel.ZitadelUserService;
import shop.velox.user_orchestration.zitadel.dto.AddGrantResponseDto;
import shop.velox.user_orchestration.zitadel.dto.AddUserGrantRequestDto;
import shop.velox.user_orchestration.zitadel.dto.grants.SearchUserGrantsResponseDto.ResultDTO;
import shop.velox.user_orchestration.zitadel.dto.unit.ZitadelUnitDto;
import shop.velox.user_orchestration.zitadel.dto.user.CreateHumanUserRequestDto;
import shop.velox.user_orchestration.zitadel.dto.user.CreateHumanUserResponseDto;
import shop.velox.user_orchestration.zitadel.dto.user.EmailDto;
import shop.velox.user_orchestration.zitadel.dto.user.HumanUserDto;
import shop.velox.user_orchestration.zitadel.dto.user.ProfileDto;
import shop.velox.user_orchestration.zitadel.dto.user.UserContainerDto;
import static java.util.Collections.emptyMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import static org.apache.commons.lang3.StringUtils.isBlank;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import shop.velox.user.api.controller.client.UserApiClient;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user_orchestration.api.dto.UserDto;

@Service
@RequiredArgsConstructor
@Slf4j
public class ZitadelUserServiceImpl implements ZitadelUserService {

  private final UpdateUserConverter updateUserConverter;
  private final UserConverter userConverter;

  private final ZitadelManagementApiClient zitadelManagementApiClient;
  private final ZitadelAuthApiClient zitadelAuthApiClient;

  private final ZitadelConfiguration zitadelConfiguration;

  private final UserApiClient userApiClient;

  @Override
  public UserDto upsertHumanUser(shop.velox.user.api.dto.user.UserDto userDto,
      ZitadelUnitDto zitadelUnitDto, List<String> roles) {
    String userCode = userDto.getCode();
    String userExternalId = userDto.getExternalId();

    UnitDto unitDto = zitadelUnitDto.getUnitDto();
    String unitExternalId = unitDto.getExternalId();
    String projectGrantId = zitadelUnitDto.getProjectGrantId();

    boolean isHumanUserNew = isBlank(userExternalId);
    if (isHumanUserNew) {
      log.info("User externalId is blank for user: {}", userCode);
      CreateHumanUserResponseDto createHumanUserResponseDto = zitadelManagementApiClient.createHumanUser(
          CreateHumanUserRequestDto.builder()
              .userName(userDto.getEmail())
              .profile(ProfileDto.builder()
                  .firstName(userDto.getFirstName())
                  .lastName(userDto.getLastName())
                  .nickName(userCode)
                  .preferredLanguage(userDto.getPreferredLanguage())
                  .build())
              .email(EmailDto.builder()
                  .email(userDto.getEmail())
                  .isEmailVerified(false)
                  .build())
              .build(),
          unitExternalId).getBody();
      String newUserExternalId = createHumanUserResponseDto.getUserId();
      log.info("New user externalId is {} for user: {}", newUserExternalId, userCode);
      userDto = updateUser(userDto, newUserExternalId);
    } else {
      log.info("User externalId is {} for user: {}", userExternalId, userCode);
      HumanUserDto humanUserDto = zitadelManagementApiClient.getHumanUser(
              unitExternalId, userExternalId)
          .getBody()
          .getUser();
      Assert.notNull(humanUserDto, "HumanUserDto is null");
    }

    AddGrantResponseDto addUserGrantResponseDto = addUserGrant(unitExternalId, userDto,
        roles, projectGrantId);

    log.info(
        "upsertHumanUser for User: {}-{}, UserGrant: {}",
        userDto.getCode(), userDto.getExternalId(),
        addUserGrantResponseDto.getGrantId());
    return userConverter.convert(userDto);
  }

  shop.velox.user.api.dto.user.UserDto updateUser(shop.velox.user.api.dto.user.UserDto oldUserDto,
      String newUserExternalId) {
    var updateUserDto = updateUserConverter.convert(oldUserDto, newUserExternalId);
    var userUpdatedWithExternalId = userApiClient.updateUser(oldUserDto.getCode(), updateUserDto)
        .getBody();
    log.info("Updated User: {}", userUpdatedWithExternalId);
    return userUpdatedWithExternalId;
  }

  AddGrantResponseDto addUserGrant(String organizationId,
      shop.velox.user.api.dto.user.UserDto updatedUserDto,
      List<String> roles, String projectGrantId) {
    String projectId = zitadelConfiguration.getProjectId();
    String userCode = updatedUserDto.getCode();
    String userExternalId = updatedUserDto.getExternalId();

    AddUserGrantRequestDto addUserGrantDto = AddUserGrantRequestDto.builder()
        .projectId(projectId)
        .projectGrantId(projectGrantId)
        .roleKeys(roles)
        .build();
    log.info("Assigning roles: {} to user with code: {} and externalId: {} with projectGrantId: {}",
        roles, userCode, userExternalId, projectGrantId);
    return zitadelManagementApiClient.addUserGrant(userExternalId, organizationId, addUserGrantDto)
        .getBody();
  }

  @Override
  public UserContainerDto getOwnUser() {
    ResponseEntity<UserContainerDto> humanUser = zitadelAuthApiClient.getOwnUser();
    return humanUser.getBody();
  }

  @Override
  public Map<String, List<String>> getUsersRoles(String orgId) {
    String projectId = zitadelConfiguration.getProjectId();
    List<ResultDTO> resultDTOS = zitadelManagementApiClient.searchUserGrants(orgId, projectId)
        .getBody()
        .getResult();
    if (resultDTOS == null) {
      log.warn("No user grants found for orgId: {}", orgId);
      return emptyMap();
    }
    Map<String, List<String>> rolesMap = resultDTOS
        .stream()
        .collect(Collectors.toMap(
            ResultDTO::getUserId,
            ResultDTO::getRoleKeys)
        );
    log.debug("RolesMap for orgId {}: {}", orgId, rolesMap);
    return rolesMap;
  }

}
