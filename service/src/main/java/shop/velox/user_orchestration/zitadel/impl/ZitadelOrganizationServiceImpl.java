package shop.velox.user_orchestration.zitadel.impl;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isBlank;

import shop.velox.user_orchestration.ZitadelConfiguration;
import shop.velox.user_orchestration.converter.UpdateUnitConverter;
import shop.velox.user_orchestration.zitadel.ZitadelManagementApiClient;
import shop.velox.user_orchestration.zitadel.ZitadelOrganizationService;
import shop.velox.user_orchestration.zitadel.dto.AddProjectGrantRequestDto;
import shop.velox.user_orchestration.zitadel.dto.grants.SearchProjectGrantResponseDto.ProjectGrant;
import shop.velox.user_orchestration.zitadel.dto.organization.CreateOrganizationRequestDto;
import shop.velox.user_orchestration.zitadel.dto.unit.ZitadelUnitDto;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import shop.velox.user.api.controller.client.UnitApiClient;
import shop.velox.user.api.dto.unit.UnitDto;

@Service
@RequiredArgsConstructor
@Slf4j
public class ZitadelOrganizationServiceImpl implements ZitadelOrganizationService {

  private final UnitApiClient unitApiClient;

  private final ZitadelManagementApiClient zitadelManagementApiClient;

  private final UpdateUnitConverter updateUnitConverter;

  private final ZitadelConfiguration zitadelConfiguration;

  public ZitadelUnitDto upsertOrganization(UnitDto unitDto) {
    String unitCode = unitDto.getCode();
    String unitExternalId = unitDto.getExternalId();

    String organizationId;
    boolean isOrganizationNew = isBlank(unitExternalId);
    if (isOrganizationNew) {
      log.info("Unit externalId is blank for unit: {}. Organization needs to be created", unitCode);
      var createOrganizationResponseDto = zitadelManagementApiClient.createOrganization(
              CreateOrganizationRequestDto.builder()
                  .name(unitCode)
                  .build())
          .getBody();
      organizationId = createOrganizationResponseDto.getId();
    } else {
      log.info("Unit externalId is {} for unit: {}. Organization should already exist.",
          unitExternalId, unitCode);
      var organizationContainerDto = zitadelManagementApiClient.getOrganization(unitExternalId)
          .getBody();
      organizationId = organizationContainerDto.getOrg().getId();
      // TODO Handle organization not found
    }

    UnitDto updatedUnitDto = updateUnit(unitDto, organizationId);

    String projectGrantId = upsertProjectGrant(organizationId);

    log.info("upsertOrganization for Organization: {}-{}, ProjectGrant: {}",
        updatedUnitDto.getCode(), updatedUnitDto.getExternalId(), projectGrantId);

    return ZitadelUnitDto.builder()
        .unitDto(updatedUnitDto)
        .projectGrantId(projectGrantId)
        .build();
  }

  private UnitDto updateUnit(UnitDto unitDto, String organizationId) {
    String unitCode = unitDto.getCode();
    String unitExternalId = unitDto.getExternalId();
    if (!StringUtils.equals(unitExternalId, organizationId)) {
      log.info(
          "organizationId: {} and unitExternalId: {} are not equal for unit: {}. Unit needs to be updated.",
          organizationId, unitExternalId, unitCode);
      var updateUnitDto = updateUnitConverter.convert(unitDto, organizationId);
      var updatedUnitDto = unitApiClient.updateUnit(unitCode, updateUnitDto).getBody();
      log.info("Updated Unit: {}", updatedUnitDto);
      return updatedUnitDto;
    } else {
      log.info(
          "organizationId and unitExternalId: are equal ({}) for unit: {}. Unit needs does not need to be updated.",
          organizationId, unitCode);
      return unitDto;
    }
  }

  private String upsertProjectGrant(String organizationId) {
    String projectId = zitadelConfiguration.getProjectId();
    String mainOrgId = zitadelConfiguration.getMainOrgId();
    Optional<ProjectGrant> projectGrantOptional = getProjectGrant(organizationId);
    List<String> projectRolesToAssign = zitadelConfiguration.getProjectRoles();

    if (projectGrantOptional.isEmpty()) {
      return createProjectGrant(organizationId, projectRolesToAssign, projectId, mainOrgId);
    } else {
      ProjectGrant existingProjectGrant = projectGrantOptional.get();
      String existingProjectGrantId = existingProjectGrant.getGrantId();
      if (CollectionUtils.isEqualCollection(existingProjectGrant.getGrantedRoleKeys(),
          projectRolesToAssign)) {
        log.info("Project: {} already granted to Organization: {} with correct roles: {}",
            projectId, organizationId, projectRolesToAssign);
        return existingProjectGrantId;
      } else {
        log.info(
            "Project: {} already granted to Organization: {} with incorrect roles: {}. Updating roles to: {}",
            projectId, organizationId, existingProjectGrant.getGrantedRoleKeys(),
            projectRolesToAssign);
        updateProjectGrant(projectId, existingProjectGrantId, projectRolesToAssign);
        return existingProjectGrantId;
      }
    }
  }

  private String createProjectGrant(String organizationId, List<String> projectRolesToAssign,
      String projectId, String mainOrgId) {
    AddProjectGrantRequestDto addProjectGrantDto = AddProjectGrantRequestDto.builder()
        .grantedOrgId(organizationId)
        .roleKeys(projectRolesToAssign)
        .build();
    log.info("Granting Project: {} to Organization: {}", projectId, organizationId);
    try {
      return zitadelManagementApiClient.addProjectGrant(projectId, addProjectGrantDto, mainOrgId)
          .getBody().getGrantId();
    } catch (HttpClientErrorException.Conflict e) {
      log.info("Project: {} already granted to Organization: {}", projectId, organizationId);
      return null;
    }
  }

  Optional<ProjectGrant> getProjectGrant(String unitExternalId) {
    String mainOrgId = zitadelConfiguration.getMainOrgId();
    List<ProjectGrant> searchProjectGrants = zitadelManagementApiClient.searchProjectGrant(
        unitExternalId,
        mainOrgId).getBody().getResult();
    if (searchProjectGrants.size() >= 2) {
      log.error("Expected at most one project grant for unit: {}, but found: {} : {}",
          unitExternalId, searchProjectGrants.size(),
          searchProjectGrants.stream().map(ProjectGrant::getGrantId).toList());
    }
    return isEmpty(searchProjectGrants) ? Optional.empty()
        : Optional.of(searchProjectGrants.getFirst());
  }

  void updateProjectGrant(String grantedOrgId, String existingProjectGrantId, List<String> roles) {
    String projectId = zitadelConfiguration.getProjectId();
    String mainOrgId = zitadelConfiguration.getMainOrgId();
    log.info("Updating ProjectGrant: for Organization {} with roles: {}", grantedOrgId, roles);
    zitadelManagementApiClient.updateProjectGrant(projectId, existingProjectGrantId, roles,
        mainOrgId);
  }

}
