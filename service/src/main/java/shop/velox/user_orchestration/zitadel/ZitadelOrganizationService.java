package shop.velox.user_orchestration.zitadel;

import shop.velox.user_orchestration.zitadel.dto.unit.ZitadelUnitDto;
import shop.velox.user.api.dto.unit.UnitDto;

public interface ZitadelOrganizationService {

  ZitadelUnitDto upsertOrganization(UnitDto unitDto);

}
