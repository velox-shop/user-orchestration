package shop.velox.user_orchestration.zitadel.dto.grants;

import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@FieldNameConstants
public class SearchProjectGrantRequestDto {

  QueryPagination query;

  List<Query> queries;

  @Value
  @Builder
  @Jacksonized
  @FieldNameConstants
  static class QueryPagination {

    @Builder.Default
    String offset = "0";

    @Builder.Default
    Integer limit = 0;

    @Builder.Default
    Boolean asc = false;

  }

  @Value
  @Builder
  @Jacksonized
  static class Query {

    QueryField grantedOrgIdQuery;
  }

  @Value
  @Builder
  @Jacksonized
  static class QueryField {

    String grantedOrgId;
  }

  public static SearchProjectGrantRequestDto create(String grantedOrgId) {
    return SearchProjectGrantRequestDto.builder()
        .queries(List.of(Query.builder()
            .grantedOrgIdQuery(QueryField.builder()
                .grantedOrgId(grantedOrgId)
                .build())
            .build()))
        .build();
  }
}
