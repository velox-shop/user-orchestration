package shop.velox.user_orchestration.zitadel.dto.grants;

import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@FieldNameConstants
public class SearchProjectGrantResponseDto {

  @Builder.Default
  List<ProjectGrant> result = new ArrayList<>();

  @Value
  @Builder
  @Jacksonized
  public static class ProjectGrant {

    String grantId;

    List<String> grantedRoleKeys;
  }
}
