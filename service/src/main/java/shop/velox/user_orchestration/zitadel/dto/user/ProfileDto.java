package shop.velox.user_orchestration.zitadel.dto.user;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@FieldNameConstants
public class ProfileDto {

  String firstName;

  String lastName;

  String nickName;

  String preferredLanguage;
}
