package shop.velox.user_orchestration.zitadel.dto.unit;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.user.api.dto.unit.UnitDto;

@Value
@Builder
@Jacksonized
@FieldNameConstants
public class ZitadelUnitDto {

  UnitDto unitDto;

  String projectGrantId;

}
