package shop.velox.user_orchestration.zitadel.dto.user;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@FieldNameConstants
public class UserContainerDto {

  DetailDto detailDto;

  HumanUserDto user;

}
