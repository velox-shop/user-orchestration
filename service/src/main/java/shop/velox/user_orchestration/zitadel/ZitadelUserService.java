package shop.velox.user_orchestration.zitadel;

import shop.velox.user_orchestration.zitadel.dto.unit.ZitadelUnitDto;
import shop.velox.user_orchestration.zitadel.dto.user.UserContainerDto;
import java.util.List;
import java.util.Map;
import shop.velox.user_orchestration.api.dto.UserDto;

public interface ZitadelUserService {

  UserDto upsertHumanUser(shop.velox.user.api.dto.user.UserDto userDto, ZitadelUnitDto unitDto,
      List<String> roles);

  UserContainerDto getOwnUser();

  Map<String, List<String>> getUsersRoles(String orgId);
}
