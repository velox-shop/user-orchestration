package shop.velox.user_orchestration.zitadel.dto.grants;

import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@FieldNameConstants
public class SearchUserGrantsRequestDto {

  QueryDTO query;
  List<QueriesDTO> queries;

  @Value
  @Builder
  @Jacksonized
  @FieldNameConstants
  public static class QueryDTO {
    String offset;
    int limit;
    boolean asc;
  }

  @Value
  @Builder
  @Jacksonized
  @FieldNameConstants
  public static class QueriesDTO {
    ProjectIdQueryDTO projectIdQuery;
  }

  @Value
  @Builder
  @Jacksonized
  @FieldNameConstants
  public static class ProjectIdQueryDTO {
    String projectId;
  }

}
