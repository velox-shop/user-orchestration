package shop.velox.user_orchestration.zitadel;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static org.springframework.http.HttpMethod.PUT;

import shop.velox.user_orchestration.converter.JsonToStringConverter;
import shop.velox.user_orchestration.zitadel.dto.AddGrantResponseDto;
import shop.velox.user_orchestration.zitadel.dto.AddProjectGrantRequestDto;
import shop.velox.user_orchestration.zitadel.dto.AddUserGrantRequestDto;
import shop.velox.user_orchestration.zitadel.dto.UpdateProjectGrantRequestDto;
import shop.velox.user_orchestration.zitadel.dto.grants.SearchProjectGrantRequestDto;
import shop.velox.user_orchestration.zitadel.dto.grants.SearchProjectGrantResponseDto;
import shop.velox.user_orchestration.zitadel.dto.grants.SearchUserGrantsRequestDto;
import shop.velox.user_orchestration.zitadel.dto.grants.SearchUserGrantsResponseDto;
import shop.velox.user_orchestration.zitadel.dto.organization.CreateOrganizationRequestDto;
import shop.velox.user_orchestration.zitadel.dto.organization.CreateOrganizationResponseDto;
import shop.velox.user_orchestration.zitadel.dto.organization.OrganizationContainerDto;
import shop.velox.user_orchestration.zitadel.dto.user.CreateHumanUserRequestDto;
import shop.velox.user_orchestration.zitadel.dto.user.CreateHumanUserResponseDto;
import shop.velox.user_orchestration.zitadel.dto.user.UserContainerDto;
import jakarta.annotation.PostConstruct;
import java.io.Serializable;
import java.net.URI;
import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

@RequiredArgsConstructor
@Slf4j
public class ZitadelManagementApiClient {

  public static final String X_ZITADEL_ORGID = "x-zitadel-orgid";

  private final RestTemplate restTemplate;
  private final JsonToStringConverter jsonToStringConverter;

  private final String hostUrl;
  private String baseUrl;

  @PostConstruct
  void init() {
    baseUrl = hostUrl + "/management/v1";
    log.info("ZitadelManagementApiClient initialized with baseUrl: {}", baseUrl);
  }

  public ResponseEntity<CreateOrganizationResponseDto> createOrganization(
      CreateOrganizationRequestDto createOrganization) {
    // https://zitadel.com/docs/apis/resources/mgmt/management-service-add-org

    HttpHeaders headers = new HttpHeaders();
    HttpEntity<CreateOrganizationRequestDto> httpEntity = new HttpEntity<>(createOrganization,
        headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("orgs")
        .buildAndExpand(uriPathParams)
        .toUri();

    log.info("createOrganization with URI: {} and payload: {}", uri,
        jsonToStringConverter.toString(createOrganization));

    try {
      return restTemplate.exchange(uri, POST, httpEntity, CreateOrganizationResponseDto.class);
    } catch (HttpClientErrorException.Conflict e) {
      // An Organization with this name already exists, but its id was not saved into the Unit.externalId.
      // This can happen if the User microservice was down when the organization was created.
      // There is no way to get an organization by name, so data must be manually fixed.
      log.error("Error creating organization: {} - {}", createOrganization, e.getMessage());
      throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
    }
  }

  public ResponseEntity<OrganizationContainerDto> getOrganization(String orgId) {
    //https://zitadel.com/docs/apis/resources/mgmt/management-service-get-my-org

    HttpHeaders headers = new HttpHeaders();
    headers.set(X_ZITADEL_ORGID, orgId);
    HttpEntity<Void> httpEntity = new HttpEntity<>(headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("orgs", "me")
        .buildAndExpand(uriPathParams)
        .toUri();

    log.info("getOrganization for orgId: {} with URI: {}", orgId, uri);
    return restTemplate.exchange(uri, GET, httpEntity, OrganizationContainerDto.class);
  }

  public ResponseEntity<CreateHumanUserResponseDto> createHumanUser(
      CreateHumanUserRequestDto createHumanUser, String orgId) {
    // https://zitadel.com/docs/apis/resources/mgmt/management-service-import-human-user

    HttpHeaders headers = new HttpHeaders();
    headers.set(X_ZITADEL_ORGID, orgId);
    HttpEntity<CreateHumanUserRequestDto> httpEntity = new HttpEntity<>(createHumanUser, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("users", "human", "_import")
        .buildAndExpand(uriPathParams)
        .toUri();

    log.info("createHumanUser for orgId: {} with URI: {} and payload: {}", orgId, uri,
        jsonToStringConverter.toString(createHumanUser));

    return restTemplate.exchange(uri, POST, httpEntity, CreateHumanUserResponseDto.class);
  }

  public ResponseEntity<UserContainerDto> getHumanUser(String orgId, String userId) {
    // https://zitadel.com/docs/apis/resources/mgmt/management-service-get-user-by-id

    HttpHeaders headers = new HttpHeaders();
    headers.set(X_ZITADEL_ORGID, orgId);
    HttpEntity<Void> httpEntity = new HttpEntity<>(headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of("userId", userId);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("users", "{userId}")
        .buildAndExpand(uriPathParams)
        .toUri();

    log.info("getHumanUser for orgId: {} with URI: {}", orgId, uri);
    return restTemplate.exchange(uri, GET, httpEntity, UserContainerDto.class);
  }

  public ResponseEntity<AddGrantResponseDto> addProjectGrant(String projectId,
      AddProjectGrantRequestDto addProjectGrantDto, String mainOrgId) {
    // https://zitadel.com/docs/apis/resources/mgmt/management-service-add-project-grant

    HttpHeaders headers = new HttpHeaders();
    headers.set(X_ZITADEL_ORGID, mainOrgId);
    HttpEntity<AddProjectGrantRequestDto> httpEntity = new HttpEntity<>(addProjectGrantDto,
        headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of("projectId", projectId);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("projects", "{projectId}", "grants")
        .buildAndExpand(uriPathParams)
        .toUri();

    log.info("addProjectGrant for mainOrgId: {} with URI: {} and payload: {}", mainOrgId, uri,
        jsonToStringConverter.toString(addProjectGrantDto));

    return restTemplate.exchange(uri, POST, httpEntity, AddGrantResponseDto.class);

  }

  public ResponseEntity<Void> updateProjectGrant(String projectId, String grantId,
      List<String> roles,
      String mainOrgId) {
    // https://zitadel.com/docs/apis/resources/mgmt/management-service-update-project-grant

    HttpHeaders headers = new HttpHeaders();
    headers.set(X_ZITADEL_ORGID, mainOrgId);

    UpdateProjectGrantRequestDto updateProjectGrantDto = UpdateProjectGrantRequestDto.builder()
        .roleKeys(roles)
        .build();
    HttpEntity<UpdateProjectGrantRequestDto> httpEntity = new HttpEntity<>(updateProjectGrantDto,
        headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of("projectId", projectId, "grantId",
        grantId);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("projects", "{projectId}", "grants", "{grantId}")
        .buildAndExpand(uriPathParams)
        .toUri();

    log.info("updateProjectGrant for mainOrgId: {} with URI: {} and payload: {}", mainOrgId, uri,
        jsonToStringConverter.toString(updateProjectGrantDto));

    return restTemplate.exchange(uri, PUT, httpEntity, Void.class);
  }

  public ResponseEntity<AddGrantResponseDto> addUserGrant(String userId, String orgId,
      AddUserGrantRequestDto addUserGrantDto) {

    // https://zitadel.com/docs/apis/resources/mgmt/management-service-add-project-grant
    // NOT https://zitadel.com/docs/apis/resources/mgmt/management-service-add-project-member
    // NOT https://zitadel.com/docs/apis/resources/mgmt/management-service-add-project-grant-member

    HttpHeaders headers = new HttpHeaders();
    headers.set(X_ZITADEL_ORGID, orgId);
    HttpEntity<AddUserGrantRequestDto> httpEntity = new HttpEntity<>(addUserGrantDto,
        headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of("userId", userId);

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("users", "{userId}", "grants")
        .buildAndExpand(uriPathParams)
        .toUri();

    log.info("addUserGrant for orgId: {} with URI: {} and payload: {}", orgId, uri,
        jsonToStringConverter.toString(addUserGrantDto));

    return restTemplate.exchange(uri, POST, httpEntity, AddGrantResponseDto.class);

  }

  public ResponseEntity<SearchProjectGrantResponseDto> searchProjectGrant(String grantedOrgId,
      String mainOrgId) {
    // https://zitadel.com/docs/apis/resources/mgmt/management-service-list-all-project-grants

    SearchProjectGrantRequestDto searchProjectGrantRequestDto = SearchProjectGrantRequestDto.create(
        grantedOrgId);

    HttpHeaders headers = new HttpHeaders();
    headers.set(X_ZITADEL_ORGID, mainOrgId);

    HttpEntity<SearchProjectGrantRequestDto> httpEntity = new HttpEntity<>(
        searchProjectGrantRequestDto, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("projectgrants", "_search")
        .buildAndExpand(uriPathParams)
        .toUri();

    log.info("searchProjectGrant for mainOrgId: {} with URI: {} and payload: {}", mainOrgId, uri,
        jsonToStringConverter.toString(searchProjectGrantRequestDto));

    return restTemplate.exchange(uri, POST, httpEntity, SearchProjectGrantResponseDto.class);
  }

  public ResponseEntity<SearchUserGrantsResponseDto> searchUserGrants(String orgId,
      String projectId) {
    // https://zitadel.com/docs/apis/resources/mgmt/management-service-list-users

    SearchUserGrantsRequestDto searchForUsersRequest = SearchUserGrantsRequestDto.builder()
        .query(SearchUserGrantsRequestDto.QueryDTO.builder()
            .offset("0")
            .build())
        .queries(List.of(SearchUserGrantsRequestDto.QueriesDTO.builder()
            .projectIdQuery(SearchUserGrantsRequestDto.ProjectIdQueryDTO.builder()
                .projectId(projectId)
                .build())
            .build()))
        .build();

    HttpHeaders headers = new HttpHeaders();
    headers.set(X_ZITADEL_ORGID, orgId);
    HttpEntity<SearchUserGrantsRequestDto> httpEntity = new HttpEntity<>(
        searchForUsersRequest, headers);

    Map<String, ? extends Serializable> uriPathParams = Map.of();

    URI uri = UriComponentsBuilder.fromUriString(baseUrl)
        .pathSegment("users", "grants", "_search")
        .buildAndExpand(uriPathParams)
        .toUri();

    log.info("searchUserGrants for orgId: {} with URI: {} and payload: {}", orgId, uri,
        jsonToStringConverter.toString(searchForUsersRequest));

    return restTemplate.exchange(uri, POST, httpEntity, SearchUserGrantsResponseDto.class);

  }


}
