package shop.velox.user_orchestration.api.controller.impl;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.RestController;
import shop.velox.user.api.dto.user.CreateUserDto;
import shop.velox.user.api.dto.user.UpdateUserDto;
import shop.velox.user.enumerations.UserStatus;
import shop.velox.user_orchestration.api.controller.UserController;
import shop.velox.user_orchestration.api.dto.CreateUserZitadelDto;
import shop.velox.user_orchestration.api.dto.UserDto;
import shop.velox.user_orchestration.api.dto.ZitadelUserDto;
import shop.velox.user_orchestration.service.UserIdentityService;
import shop.velox.user_orchestration.service.UserService;

@RestController
@RequiredArgsConstructor
@Slf4j
public class UserControllerImpl implements UserController {

  private final UserService userService;

  private final UserIdentityService userIdentityService;


  @Override
  public UserDto createUser(CreateUserDto user) {
    return userService.createUser(user);
  }

  @Override
  public UserDto getUser(String userCode) {
    return userService.getUser(userCode);
  }

  @Override
  public UserDto getCurrentUser() {
    return userService.getCurrentUser();
  }


  @Override
  public UserDto updateUser(String userCode, UpdateUserDto user) {
    return userService.updateUser(userCode, user);
  }

  @Override
  public Page<UserDto> getUsers(String firstName, String lastName, String email, String unitCode,
      List<String> userCodes, List<UserStatus> statuses, Pageable pageable) {
    return userService.getUsers(firstName, lastName, email, unitCode, userCodes, statuses,
        pageable);
  }

  @Override
  public UserDto upsertUserIdentity(String userCode, CreateUserZitadelDto createUserZitadelDto) {
    return userService.upsertUserIdentity(userCode, createUserZitadelDto);
  }

  @Override
  public ZitadelUserDto getOwnZitadelUser() {
    return userIdentityService.getOwnUser();
  }
}
