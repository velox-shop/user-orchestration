package shop.velox.user_orchestration.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.enumerations.UnitStatus;

@Tag(name = "Unit", description = "the Unit API")
@RequestMapping(value = "/units", produces = MediaType.APPLICATION_JSON_VALUE)
public interface UnitController {

  @Operation(summary = "Get all Units")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200", description = "Page of units")
      })
  @GetMapping
  //https://springdoc.org/faq.html#_how_can_i_map_pageable_spring_data_commons_object_to_correct_url_parameter_in_swagger_ui
  @PageableAsQueryParam
  Page<UnitDto> getUnits(
      @Parameter(description = "Optionally Filter units by name. It works also with substring matches")
      @RequestParam(name = "name", required = false) String name,

      @Parameter(description = "Optionally Filter units by code.")
      @RequestParam(name = "code", required = false) String code,

      @Parameter(description = "Optionally Filter units by status.")
      @RequestParam(name = "status", required = false, defaultValue = "ACTIVE") List<UnitStatus> statuses,

      @Parameter(description = "Optionally Filter units by user code.")
      @RequestParam(name = "userCode", required = false) String userCode,

      @Parameter(description = "Filter units by search text. It works also with substring matches")
      @RequestParam(name = "searchText", required = false) String searchText,

      @SortDefault.SortDefaults({
          @SortDefault(sort = "name", direction = Sort.Direction.ASC)
      })
      @Parameter(hidden = true) Pageable pageable);

  @Operation(summary = "Get a Unit")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Unit that was found",
              content = @Content(schema = @Schema(implementation = UnitDto.class))),
          @ApiResponse(
              responseCode = "403",
              description = "Auth missing or insufficient",
              content = @Content(schema = @Schema())),
          @ApiResponse(
              responseCode = "404",
              description = "Unit was not found",
              content = @Content(schema = @Schema())),
      })
  @GetMapping(value = "/{unitCode}")
  UnitDto getUnit(
      @Parameter(description = "Unit Code. Cannot be empty.", required = true)
      @PathVariable("unitCode") String unitCode);

  @Operation(summary = "Get the Unit that the currently logged in user belongs to in Zitadel")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Unit that was found",
              content = @Content(schema = @Schema(implementation = UnitDto.class))),
          @ApiResponse(
              responseCode = "403",
              description = "Auth missing or insufficient",
              content = @Content(schema = @Schema())),
          @ApiResponse(
              responseCode = "404",
              description = "Unit was not found",
              content = @Content(schema = @Schema())),
      })
  @GetMapping(value = "/me")
  UnitDto getZitadelUnit();

}
