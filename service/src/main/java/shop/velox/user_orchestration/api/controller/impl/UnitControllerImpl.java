package shop.velox.user_orchestration.api.controller.impl;

import shop.velox.user_orchestration.api.controller.UnitController;
import shop.velox.user_orchestration.service.UnitService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.enumerations.UnitStatus;

@RestController
@RequiredArgsConstructor
public class UnitControllerImpl implements UnitController {

  private final UnitService unitService;

  @Override
  public Page<UnitDto> getUnits(@Nullable String name, @Nullable String code,
      List<UnitStatus> statuses, String userCode, @Nullable String searchText, Pageable pageable) {
    return unitService.getUnits(name, code, statuses, userCode, searchText, pageable);
  }

  @Override
  public UnitDto getUnit(String unitCode) {
    return unitService.getUnit(unitCode);
  }

  @Override
  public UnitDto getZitadelUnit() {
    return unitService.getZitadelUnit().orElseThrow(
        () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Zitadel unit not found"));
  }

}
