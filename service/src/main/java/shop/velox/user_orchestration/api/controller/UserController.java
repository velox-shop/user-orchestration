package shop.velox.user_orchestration.api.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import java.util.List;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import shop.velox.user.api.dto.user.CreateUserDto;
import shop.velox.user.api.dto.user.UpdateUserDto;
import shop.velox.user.enumerations.UserStatus;
import shop.velox.user_orchestration.api.dto.CreateUserZitadelDto;
import shop.velox.user_orchestration.api.dto.UserDto;
import shop.velox.user_orchestration.api.dto.ZitadelUserDto;

@Tag(name = "User", description = "the User API")
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
public interface UserController {

  @Operation(summary = "Create new User")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "201",
              description = "User created"),
          @ApiResponse(
              responseCode = "409",
              description = "An user with given code already exists",
              content = @Content(schema = @Schema())),
          @ApiResponse(
              responseCode = "422",
              description = "Mandatory data missing",
              content = @Content(schema = @Schema()))
      })
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.CREATED)
  UserDto createUser(
      @Parameter(description = "User to insert. Cannot be empty.", required = true)
      @Valid @RequestBody final CreateUserDto user);

  @Operation(summary = "Find User by code", description = "")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Successful operation"),
          @ApiResponse(
              responseCode = "404",
              description = "User not found",
              content = @Content(schema = @Schema()))
      })
  @GetMapping(value = "/{code}")
  UserDto getUser(
      @Parameter(description = "Code of the User. Cannot be empty.", required = true)
      @PathVariable("code") final String userCode);

  @Operation(summary = "Get current user")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Successful operation",
              content = @Content(schema = @Schema(implementation = UserDto.class))),
          @ApiResponse(
              responseCode = "404",
              description = "Authorized user has no user account",
              content = @Content(schema = @Schema()))
      })
  @GetMapping(value = "/me")
  UserDto getCurrentUser();


  @Operation(summary = "Updates an user", description = "")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Successful operation",
              content = @Content(schema = @Schema(implementation = UserDto.class))),
          @ApiResponse(
              responseCode = "404",
              description = "User not found",
              content = @Content(schema = @Schema())),
          @ApiResponse(
              responseCode = "422",
              description = "Mandatory data missing",
              content = @Content(schema = @Schema()))
      })
  @PatchMapping(value = "/{code}", consumes = MediaType.APPLICATION_JSON_VALUE)
  UserDto updateUser(
      @Parameter(description = "Code of the User. Cannot be empty.", required = true)
      @PathVariable("code") final String userCode,

      @Parameter(description = "User to update. Cannot be empty.", required = true)
      @Valid @RequestBody final UpdateUserDto user);

  @Operation(summary = "Registers an user on Zitadel")
  @ApiResponses({
      @ApiResponse(
          responseCode = "201",
          description = "User Identity created",
          content = @Content(schema = @Schema(implementation = UserDto.class))),
      @ApiResponse(
          responseCode = "200",
          description = "User Identity updated",
          content = @Content(schema = @Schema(implementation = UserDto.class))),
      @ApiResponse(
          responseCode = "404",
          description = "User not found",
          content = @Content(schema = @Schema())),
      @ApiResponse(
          responseCode = "422",
          description = """
              user lacks some mandatory data, by example the email; 
              Or the unit is not found or inactive. 
              Or the user already has a Zitadel identity.
              Or the roles are not valid.
              """,
          content = @Content(schema = @Schema()))
  })
  @PostMapping(value = "/{code}/identity", consumes = MediaType.APPLICATION_JSON_VALUE)
  UserDto upsertUserIdentity(
      @Parameter(description = "Code of the User. Cannot be empty.", required = true)
      @PathVariable("code") final String userCode,

      @Parameter(description = "Roles for the user", required = true)
      @Valid @RequestBody final CreateUserZitadelDto createUserZitadelDto);


  @Operation(summary = "Get all Users (Paginated)", description = "")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Successful operation")
      })
  @PageableAsQueryParam
  @GetMapping
  Page<UserDto> getUsers(
      @Parameter(description = "Optionally filter Users by First name. It works also with substring matches")
      @RequestParam(name = "firstName", required = false) String firstName,

      @Parameter(description = "Optionally filter Users by Last name. It works also with substring matches")
      @RequestParam(name = "lastName", required = false) String lastName,

      @Parameter(description = "Optionally filter users by email. It works also with substring matches")
      @RequestParam(name = "email", required = false) String email,

      @Parameter(description = "Optionally filter users by Unit Code.")
      @RequestParam(name = "unitCode", required = false) String unitCode,

      @Parameter(description = "Optionally filter users by User Code.")
      @RequestParam(name = "code", required = false) List<String> userCodes,

      @Parameter(description = "Optionally Filter users by status.")
      @RequestParam(name = "status", required = false, defaultValue = "ACTIVE") List<UserStatus> statuses,
      
      @Parameter(hidden = true) Pageable pageable);

  @Operation(summary = "Get the Zitadel identity of the logged-in user")
  @ApiResponses(
      value = {
          @ApiResponse(
              responseCode = "200",
              description = "Successful operation")
      })
  @GetMapping(value = "/me/identity")
  ZitadelUserDto getOwnZitadelUser();
}
