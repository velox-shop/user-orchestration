package shop.velox.user_orchestration;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "velox.zitadel")
@Data
@Validated
public class ZitadelConfiguration {

  @NotBlank
  private String projectId;

  @NotBlank
  private String mainOrgId;

  @NotNull
  @NotEmpty
  private List<String> projectRoles;

  @NotNull
  @NotEmpty
  private List<String> defaultUserRoles;

}
