package shop.velox.user_orchestration.converter;

import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import shop.velox.user.api.dto.user.UpdateUserDto;
import shop.velox.user.api.dto.user.UserDto;

@Mapper
public interface UpdateUserConverter {

  @Mappings(
      @Mapping(target = UpdateUserDto.Fields.externalId, ignore = true)
  )
  UpdateUserDto convert(UserDto userDto, @Context String contextExternalId);

  @AfterMapping
  default void fillExternalId(UserDto userDto, @Context String contextExternalId,
      @MappingTarget UpdateUserDto.UpdateUserDtoBuilder updateUserDtoBuilder) {
    updateUserDtoBuilder.externalId(contextExternalId);
  }

}
