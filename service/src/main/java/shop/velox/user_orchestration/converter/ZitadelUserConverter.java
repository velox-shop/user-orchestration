package shop.velox.user_orchestration.converter;

import shop.velox.user_orchestration.zitadel.dto.user.UserContainerDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import shop.velox.user_orchestration.api.dto.ZitadelUserDto;

@Mapper
public interface ZitadelUserConverter {

  @Mappings({
      @Mapping(target = "id", source = "user.id"),
      @Mapping(target = "userName", source = "user.userName"),
      @Mapping(target = "nickName", source = "user.human.profile.nickName"),
      @Mapping(target = "unitExternalId", source = "user.details.resourceOwner"),
      // TODO map externalId to UnitCode
  })
  ZitadelUserDto convert(UserContainerDto userContainerDto);

}
