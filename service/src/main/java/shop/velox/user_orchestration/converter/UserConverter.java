package shop.velox.user_orchestration.converter;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import static org.apache.commons.collections4.MapUtils.isEmpty;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.lang.Nullable;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.enumerations.UnitStatus;
import shop.velox.user_orchestration.api.dto.UserDto;

@Mapper
public interface UserConverter {

  Logger log = LoggerFactory.getLogger(UserConverter.class);

  @Mappings({
      @Mapping(target = "roles", ignore = true),
      @Mapping(target = "unitStatus", ignore = true),
  })
  UserDto convert(shop.velox.user.api.dto.user.UserDto serviceUserDto);


  @Mappings({
      @Mapping(target = "roles", source = "serviceUserDto", qualifiedByName = "getRoles"),
      @Mapping(target = "unitStatus", source = "serviceUserDto", qualifiedByName = "getUnitStatus"),
  })
  UserDto convert(shop.velox.user.api.dto.user.UserDto serviceUserDto,
      @Context @Nullable UnitDto unit,
      @Context Map<String, List<String>> userRolesMap);

  @Named("getRoles")
  default @Nullable List<String> getRoles(shop.velox.user.api.dto.user.UserDto serviceUserDto,
      @Context Map<String, List<String>> userRolesMap) {
    if (isEmpty(userRolesMap)) {
      log.warn("For User: {} in units: {}, UserRolesMap is empty",
          serviceUserDto.getCode(), serviceUserDto.getUnitCodes());
      return null;
    }
    return userRolesMap.getOrDefault(serviceUserDto.getExternalId(), List.of());
  }

  @Named("getUnitStatus")
  default @Nullable UnitStatus getMainUnitStatus(
      shop.velox.user.api.dto.user.UserDto serviceUserDto,
      @Nullable @Context UnitDto unit) {
    return Optional.ofNullable(unit)
        .map(UnitDto::getStatus)
        .orElse(null);
  }

}
