package shop.velox.user_orchestration.converter;

import org.mapstruct.AfterMapping;
import org.mapstruct.Context;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.mapstruct.Mappings;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.api.dto.unit.UpdateUnitDto;

@Mapper
public interface UpdateUnitConverter {

  @Mappings({
      @Mapping(target = UpdateUnitDto.Fields.externalId, ignore = true),
  })
  UpdateUnitDto convert(UnitDto unitDto, @Context String contextExternalId);

  @AfterMapping
  default void fillExternalId(UnitDto unitDto, @Context String contextExternalId,
      @MappingTarget UpdateUnitDto.UpdateUnitDtoBuilder updateUnitDtoBuilder) {
    updateUnitDtoBuilder.externalId(contextExternalId);
  }

}
