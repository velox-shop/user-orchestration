package shop.velox.user_orchestration.converter;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
public class JsonToStringConverter {

  private final ObjectMapper objectMapper;

  public String toString(Object object) {
    try {
      return objectMapper.writeValueAsString(object);
    } catch (Exception e) {
      log.error("Error converting object: {} to string", object, e);
      return object.toString();
    }
  }

}
