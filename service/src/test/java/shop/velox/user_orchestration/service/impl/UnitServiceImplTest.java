package shop.velox.user_orchestration.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class UnitServiceImplTest {

  public static Stream<Arguments> getSearchTextsTestData() {
    return Stream.of(
        Arguments.of("Empty searchText", "", List.of()),
        Arguments.of("Single searchText", "searchText", List.of("searchText")),
        Arguments.of("Multiple searchTexts", "searchText1 searchText2", List.of("searchText1", "searchText2")),
        Arguments.of("Multiple searchTexts with leading space", " searchText1 searchText2", List.of("searchText1", "searchText2")),
        Arguments.of("Multiple searchTexts with trailing space", "searchText1 searchText2 ", List.of("searchText1", "searchText2")),
        Arguments.of("Multiple searchTexts with leading and trailing space", " searchText1 searchText2 ", List.of("searchText1", "searchText2")),
        Arguments.of("Multiple searchTexts with multiple spaces", " searchText1  searchText2 ", List.of("searchText1", "searchText2")),
        Arguments.of("Multiple searchTexts with multiple spaces and leading and trailing space", "  searchText1  searchText2  ", List.of("searchText1", "searchText2"))
    );
  }

  @ParameterizedTest(name = "{0}")
  @DisplayName("searchText is properly split")
  @MethodSource("getSearchTextsTestData")
  void getSearchTextsTest(String testName, String searchText, List<String> expected) {

    var actual = UnitServiceImpl.getSearchTexts(searchText);
    assertThat(actual).isEqualTo(expected);

  }
}
