package shop.velox.user_orchestration.service.impl;

import jakarta.annotation.Resource;
import java.util.List;
import java.util.stream.Stream;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.function.Executable;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.server.ResponseStatusException;
import shop.velox.user_orchestration.AbstractIntegrationTest;

@TestPropertySource(properties = {
    "velox.zitadel.project-roles=UnitAdmin,UnitUser,UnitRestrictedUser,BranchOrderUser"
})
class UserServiceImplTest extends AbstractIntegrationTest {

  private static final String SALES_USER = "SalesUser";
  private static final String UNIT_ADMIN = "UnitAdmin";
  private static final String UNIT_USER = "UnitUser";
  private static final String UNIT_RESTRICTED_USER = "UnitRestrictedUser";
  private static final String BRANCH_ORDER_USER = "BranchOrderUser";

  @Resource
  UserServiceImpl userService;

  @ParameterizedTest
  @MethodSource("validateRolesTestData")
  void validateRolesTest(List<String> roles, boolean isValid) {
    Executable validationExecutable = () -> userService.validateRoles("userCode", roles);
    if(isValid) {
      assertDoesNotThrow(validationExecutable);
    } else {
      assertThrows(ResponseStatusException.class, validationExecutable);
    }
  }

  public static Stream<Arguments> validateRolesTestData() {
    return Stream.of(
        Arguments.of(List.of(), true), // if empty List, defaults will be applied
        Arguments.of(List.of(UNIT_USER), true),
        Arguments.of(List.of(UNIT_ADMIN), true),
        Arguments.of(List.of(UNIT_RESTRICTED_USER), true),
        Arguments.of(List.of(UNIT_USER, UNIT_USER), false),
        Arguments.of(List.of(BRANCH_ORDER_USER, UNIT_USER), true),
        Arguments.of(List.of(BRANCH_ORDER_USER, UNIT_ADMIN), true),
        Arguments.of(List.of(BRANCH_ORDER_USER, UNIT_RESTRICTED_USER), true),
        Arguments.of(List.of(BRANCH_ORDER_USER, UNIT_USER, SALES_USER), false),
        Arguments.of(List.of(BRANCH_ORDER_USER, UNIT_ADMIN, "Unknown_Role"), false),
        Arguments.of(List.of(UNIT_ADMIN, "Unknown_Role"), false),
        Arguments.of(List.of("Unknown_Role"), false)
    );
  }

}
