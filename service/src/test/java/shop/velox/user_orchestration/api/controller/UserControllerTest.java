package shop.velox.user_orchestration.api.controller;

import shop.velox.user_orchestration.AbstractWebIntegrationTest;
import shop.velox.user_orchestration.service.impl.UserIdentityServiceImpl;
import shop.velox.user_orchestration.zitadel.ZitadelAuthApiClient;
import shop.velox.user_orchestration.zitadel.dto.user.HumanUserDto;
import shop.velox.user_orchestration.zitadel.dto.user.HumanUserDto.Human;
import shop.velox.user_orchestration.zitadel.dto.user.HumanUserDto.Profile;
import shop.velox.user_orchestration.zitadel.dto.user.UserContainerDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.Resource;
import java.util.List;
import static org.apache.commons.collections4.MapUtils.emptyIfNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ProblemDetail;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.web.client.HttpClientErrorException;
import shop.velox.user.api.dto.user.UserDto;
import shop.velox.user.enumerations.UnitStatus;
import shop.velox.user_orchestration.api.client.UserApiClient;
import shop.velox.user_orchestration.api.dto.CreateUserZitadelDto;
import shop.velox.user_orchestration.api.dto.ZitadelUserDto;

class UserControllerTest {

  @Nested
  class GetOwnZitadelIdentity extends AbstractWebIntegrationTest {

    @MockitoBean
    ZitadelAuthApiClient zitadelAuthApiClient;

    @MockitoBean
    UserApiClient userApiClient;

    @MockitoBean
    shop.velox.user.api.controller.client.UserApiClient serviceUserApiClient;

    @MockitoBean
    shop.velox.user.api.controller.client.UnitApiClient serviceUnitApiClient;

    @Resource
    ObjectMapper objectMapper;

    @Test
    void getHumanUser() {
      // Given

      String userCode = "6849";
      String email = "someone@example.com";
      String userExternalId = "276416241990663778";

      when(serviceUserApiClient.getUser(userCode))
          .thenReturn(ResponseEntity.ok(
              UserDto.builder()
                  .code(userCode)
                  .externalId(userExternalId)
                  .email(email)
                  .preferredLanguage("de")
                  .unitCodes(List.of("388002"))
                  .build()));

      when(serviceUnitApiClient.getUnit("388002"))
          .thenReturn(ResponseEntity.ok(
              shop.velox.user.api.dto.unit.UnitDto.builder()
                  .code("388002")
                  .externalId("276416241504134726")
                  .build()));

      UserContainerDto userContainerDto = UserContainerDto.builder()
          .user(HumanUserDto.builder()
              .id(userExternalId)
              .userName(email)
              .human(Human.builder()
                  .profile(Profile.builder()
                      .nickName(userCode)
                      .build())
                  .build())
              .build())
          .build();
      ResponseEntity<UserContainerDto> userContainerDtoResponseEntity = ResponseEntity.ok(
          userContainerDto);
      when(zitadelAuthApiClient.getOwnUser()).thenReturn(userContainerDtoResponseEntity);

      // When
      var responseEntity = getAnonymousUserApiClient().getOwnZitadelIdentity();

      // Then
      assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
      ZitadelUserDto actualDto = responseEntity.getBody();
      assertThat(actualDto).isNotNull();
      assertThat(actualDto.getId()).isEqualTo(userExternalId);
      assertThat(actualDto.getUserName()).isEqualTo(email);
      assertThat(actualDto.getNickName()).isEqualTo(userCode);

    }
  }

  @Nested
  @TestPropertySource(properties = {
      "logging.level.shop.velox.user_orchestration.api.controller=DEBUG",
      "server.error.include-message=always",
  })
  class CreateZitadelIdentity extends AbstractWebIntegrationTest {

    @MockitoBean
    ZitadelAuthApiClient zitadelAuthApiClient;

    @MockitoBean
    UserApiClient userApiClient;

    @MockitoBean
    UserIdentityServiceImpl userIdentityService;

    @MockitoBean
    shop.velox.user.api.controller.client.UserApiClient serviceUserApiClient;

    @MockitoBean
    shop.velox.user.api.controller.client.UnitApiClient serviceUnitApiClient;

    @Test
    @DisplayName("Creating User Identity in a Unit of which the user is not a member should fail")
    void createUserIdentityWithOtherUnitCode() {
      // Given
      String userCode = "6849";
      String email = "someone@example.com";
      String unitCode = "388002";

      when(serviceUserApiClient.getUser(userCode))
          .thenReturn(ResponseEntity.ok(
              UserDto.builder()
                  .code(userCode)
                  .email(email)
                  .preferredLanguage("de")
                  .unitCodes(List.of(unitCode))
                  .build()));

      CreateUserZitadelDto createUserZitadelDto = CreateUserZitadelDto.builder()
          .unitCode("OTHER_UNIT_CODE")
          .build();

      // When
      var exception = assertThrows(HttpClientErrorException.UnprocessableEntity.class,
          () -> getAdminUserApiClient().createZitadelIdentity(userCode, createUserZitadelDto));

      // Then
      var response = exception.getResponseBodyAs(ProblemDetail.class);
      assertThat(response).isNotNull();
      assertThat(getErrorMessage(response)).isEqualTo("User is not in Unit");

      verifyNoInteractions(userIdentityService);
    }

    @Test
    @DisplayName("Creating User Identity in a Unit which does not exist should fail")
    void createUserIdentityWithNotExistingUnitCode() {
      // Given
      String userCode = "6849";
      String email = "someone@example.com";
      String unitCode = "388002";

      when(serviceUserApiClient.getUser(userCode))
          .thenReturn(ResponseEntity.ok(
              UserDto.builder()
                  .code(userCode)
                  .email(email)
                  .preferredLanguage("de")
                  .unitCodes(List.of(unitCode))
                  .build()));

      when(serviceUnitApiClient.getUnit(unitCode)).thenThrow(
          HttpClientErrorException.NotFound.class);

      CreateUserZitadelDto createUserZitadelDto = CreateUserZitadelDto.builder()
          .unitCode(unitCode)
          .build();

      // When
      var exception = assertThrows(HttpClientErrorException.UnprocessableEntity.class,
          () -> getAdminUserApiClient().createZitadelIdentity(userCode, createUserZitadelDto));

      // Then
      var response = exception.getResponseBodyAs(ProblemDetail.class);
      assertThat(response).isNotNull();
      assertThat(getErrorMessage(response)).isEqualTo("Unit does not exist");

      verifyNoInteractions(userIdentityService);
    }

    @Test
    @DisplayName("Creating User Identity for a User without email address should fail")
    void createUserIdentityWithoutEmail() {
      // Given
      String userCode = "6849";
      String email = null;
      String unitCode = "388002";

      when(serviceUserApiClient.getUser(userCode))
          .thenReturn(ResponseEntity.ok(
              UserDto.builder()
                  .code(userCode)
                  .email(email)
                  .preferredLanguage("de")
                  .unitCodes(List.of("388002"))
                  .build()));
      CreateUserZitadelDto createUserZitadelDto = CreateUserZitadelDto.builder()
          .unitCode(unitCode)
          .build();

      // When
      var exception = assertThrows(HttpClientErrorException.UnprocessableEntity.class,
          () -> getAdminUserApiClient().createZitadelIdentity(userCode, createUserZitadelDto));

      // Then
      var response = exception.getResponseBodyAs(ProblemDetail.class);
      assertThat(response).isNotNull();
      assertThat(getErrorMessage(response)).isEqualTo("User has no email address");

      verifyNoInteractions(userIdentityService);
    }

    @Test
    @DisplayName("Creating User Identity in a Unit which is inactive should fail")
    void createUserIdentityWithInactiveUnit() {
      // Given
      String userCode = "6849";
      String email = "someone@example.com";
      String unitCode = "388002";

      when(serviceUserApiClient.getUser(userCode))
          .thenReturn(ResponseEntity.ok(
              UserDto.builder()
                  .code(userCode)
                  .email(email)
                  .preferredLanguage("de")
                  .unitCodes(List.of(unitCode))
                  .build()));

      when(serviceUnitApiClient.getUnit(unitCode)).thenReturn(ResponseEntity.ok(
          shop.velox.user.api.dto.unit.UnitDto.builder()
              .code(unitCode)
              .externalId("12345")
              .status(UnitStatus.INACTIVE)
              .build()));

      CreateUserZitadelDto createUserZitadelDto = CreateUserZitadelDto.builder()
          .unitCode(unitCode)
          .build();

      // When
      var exception = assertThrows(HttpClientErrorException.UnprocessableEntity.class,
          () -> getAdminUserApiClient().createZitadelIdentity(userCode, createUserZitadelDto));

      // Then
      var response = exception.getResponseBodyAs(ProblemDetail.class);
      assertThat(response).isNotNull();
      assertThat(getErrorMessage(response)).isEqualTo("Unit is inactive");

      verifyNoInteractions(userIdentityService);
    }

    @Test
    @DisplayName("Creating User Identity succeeds")
    void createUserIdentity() {
      // Given
      String userCode = "6849";
      String email = "someone@example.com";
      String unitCode = "388002";

      when(serviceUserApiClient.getUser(userCode))
          .thenReturn(ResponseEntity.ok(
              UserDto.builder()
                  .code(userCode)
                  .email(email)
                  .preferredLanguage("de")
                  .unitCodes(List.of(unitCode))
                  .build()));

      when(serviceUnitApiClient.getUnit(unitCode)).thenReturn(ResponseEntity.ok(
          shop.velox.user.api.dto.unit.UnitDto.builder()
              .code(unitCode)
              .externalId("12345")
              .status(UnitStatus.ACTIVE)
              .build()));

      CreateUserZitadelDto createUserZitadelDto = CreateUserZitadelDto.builder()
          .unitCode(unitCode)
          .build();

      // When
      getAdminUserApiClient().createZitadelIdentity(userCode, createUserZitadelDto);

      // Then
      verify(userIdentityService).upsertUserIdentity(eq(createUserZitadelDto), any(), any());
    }
  }

  private static String getErrorMessage(ProblemDetail response) {
    String messageKey = "message";
    return (String) emptyIfNull(response.getProperties()).get(messageKey);
  }

}
