package shop.velox.user_orchestration;


import jakarta.annotation.Resource;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.web.client.RestTemplate;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_ADMIN_PASSWORD;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_ADMIN_USERNAME;
import shop.velox.user_orchestration.api.client.UnitApiClient;
import shop.velox.user_orchestration.api.client.UserApiClient;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(OrderAnnotation.class)
@Slf4j
public abstract class AbstractWebIntegrationTest extends AbstractIntegrationTest {

  public static final String HOST_URL = "http://localhost:%s";

  @Resource
  protected RestTemplateBuilder restTemplateBuilder;

  @Value(value = "${local.server.port}")
  protected int port;

  @Getter(AccessLevel.PROTECTED)
  private RestTemplate anonymousRestTemplate;

  @Getter(AccessLevel.PROTECTED)
  private RestTemplate adminRestTemplate;

  @Getter
  private UserApiClient anonymousUserApiClient;

  @Getter
  private UserApiClient adminUserApiClient;

  @Getter
  private UnitApiClient anonymousUnitApiClient;

  @Getter
  private UnitApiClient adminUnitApiClient;

  @BeforeEach
  public void setup() {
    log.info("Web test method setup");

    anonymousRestTemplate = restTemplateBuilder.build();

    adminRestTemplate = restTemplateBuilder.basicAuthentication(LOCAL_ADMIN_USERNAME,
        LOCAL_ADMIN_PASSWORD).build();

    anonymousUserApiClient = new UserApiClient(anonymousRestTemplate,
        String.format(HOST_URL, port));

    adminUserApiClient = new UserApiClient(adminRestTemplate, String.format(HOST_URL, port));

    anonymousUnitApiClient = new UnitApiClient(anonymousRestTemplate,
        String.format(HOST_URL, port));

    adminUnitApiClient = new UnitApiClient(adminRestTemplate, String.format(HOST_URL, port));

  }

}
