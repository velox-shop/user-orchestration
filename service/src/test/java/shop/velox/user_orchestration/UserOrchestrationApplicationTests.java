package shop.velox.user_orchestration;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

@SpringBootTest
@TestPropertySource(properties = {
		"debug=true"
})
class UserOrchestrationApplicationTests extends AbstractIntegrationTest {

	@Test
	void contextLoads() {
	}

}
