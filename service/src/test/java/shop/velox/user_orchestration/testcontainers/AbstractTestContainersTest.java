package shop.velox.user_orchestration.testcontainers;

import shop.velox.user_orchestration.AbstractWebIntegrationTest;
import java.io.File;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Timeout;
import org.springframework.test.context.ActiveProfiles;
import org.testcontainers.containers.ContainerState;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Testcontainers;

@Testcontainers
@Timeout(value = 2, unit = TimeUnit.MINUTES)
@ActiveProfiles(profiles = {"localauth", "testcontainers"}, inheritProfiles = false)
@Slf4j
public abstract class AbstractTestContainersTest extends AbstractWebIntegrationTest {

  public static final String MY_SQL_SERVICE_NAME = "mysql_1";

  public static final String USER_SERVICE_NAME = "user_1";

  public static Supplier<DockerComposeContainer<?>> environmentSupplier = () ->
      new DockerComposeContainer<>(new File("src/test/resources/docker-compose.yml"))
          .withExposedService(MY_SQL_SERVICE_NAME, 3306)
          .withExposedService(USER_SERVICE_NAME, 8447)
          .waitingFor(USER_SERVICE_NAME, Wait.forHealthcheck());

  @AfterEach
  void printLogs() {
    log.info("AfterEach printLogs() method called.");

    ContainerState mySqlContainer = getEnvironment().getContainerByServiceName(
        MY_SQL_SERVICE_NAME).orElseThrow();
    log.info("mySqlContainer Logs: {}", mySqlContainer.getLogs());

    ContainerState userContainer = getEnvironment().getContainerByServiceName(
        USER_SERVICE_NAME).orElseThrow();
    log.info("userContainer Logs: {}", userContainer.getLogs());
  }

  abstract DockerComposeContainer<?> getEnvironment();

}
