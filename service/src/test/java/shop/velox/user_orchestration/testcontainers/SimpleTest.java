package shop.velox.user_orchestration.testcontainers;


import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.test.context.TestPropertySource;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.junit.jupiter.Container;

@TestPropertySource(properties = {
    "logging.level.org.testcontainers=TRACE"
})
@Slf4j
public class SimpleTest extends AbstractTestContainersTest {

  @Container
  public static DockerComposeContainer<?> environment = environmentSupplier.get();

  @Test
  @Timeout(value = 1, unit = TimeUnit.MINUTES)
  void test() {
    log.info("SimpleTest test method called.");
    assertTrue(true);
  }

  @Override
  DockerComposeContainer<?> getEnvironment() {
    return environment;
  }
}
