package shop.velox.user_orchestration.testcontainers;

import jakarta.annotation.Resource;
import java.util.List;
import lombok.SneakyThrows;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestMethodOrder;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import static org.springframework.http.HttpStatus.OK;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.bean.override.mockito.MockitoBean;
import org.springframework.web.client.ResourceAccessException;
import org.testcontainers.containers.DockerComposeContainer;
import org.testcontainers.junit.jupiter.Container;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_ADMIN_PASSWORD;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_ADMIN_USERNAME;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_1_USERNAME;
import static shop.velox.commons.VeloxLocalWebSecurityConfiguration.LOCAL_USER_2_USERNAME;
import shop.velox.user.api.controller.client.UnitApiClient;
import shop.velox.user.api.controller.client.UserApiClient;
import shop.velox.user.api.dto.unit.CreateUnitDto;
import shop.velox.user.api.dto.unit.UnitDto;
import shop.velox.user.api.dto.user.CreateUserDto;
import shop.velox.user.enumerations.UnitStatus;
import shop.velox.user.enumerations.UserStatus;
import shop.velox.user_orchestration.api.client.UnitApiClient.GetUnitsFilters;
import shop.velox.user_orchestration.api.client.UserApiClient.GetUsersFilters;
import shop.velox.user_orchestration.service.UserConfiguration;
import shop.velox.user_orchestration.zitadel.ZitadelManagementApiClient;
import shop.velox.user_orchestration.zitadel.dto.grants.SearchUserGrantsResponseDto;
import shop.velox.user_orchestration.zitadel.dto.grants.SearchUserGrantsResponseDto.ResultDTO;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(OrderAnnotation.class)
@TestPropertySource(properties = {
    "logging.level.org.springframework.web.client=TRACE",
    "logging.level.sly.velox.user_orchestration.api.client=DEBUG",
    "logging.level.sly.velox.user_orchestration.api.controller=DEBUG",
    "logging.level.shop.velox.user.api.controller.client=DEBUG",
})
public class SearchUnitsAndUsersContainerTest extends AbstractTestContainersTest {

  private static final Logger log = LoggerFactory.getLogger(SearchUnitsAndUsersContainerTest.class);

  public static final String UNIT_CODE_1 = "unitCode1";
  public static final String UNIT_NAME_1 = "unitNäme1";
  public static final String UNIT_1_SEARCH_TEXT = String.join(" ", UNIT_CODE_1, UNIT_NAME_1,
      "Zürich");

  public static final String UNIT_CODE_2 = "unitCode2";
  public static final String UNIT_NAME_2 = "unitName2";
  public static final String UNIT_2_EXTERNAL_ID = "unit2ExternalId";

  public static final String USER_3_CODE = "user3Code+foo";
  public static final String USER_3_EXTERNAL_ID = "user3ExternalId";

  @Container
  public static DockerComposeContainer<?> environment = environmentSupplier.get();

  @Override
  DockerComposeContainer<?> getEnvironment() {
    return environment;
  }

  @Resource
  private UserConfiguration userConfiguration;

  @MockitoBean
  ZitadelManagementApiClient zitadelManagementApiClient;

  @BeforeAll
  @SneakyThrows
  void beforeAll() {
    log.info("beforeAll() method called.");
    String userUrl = userConfiguration.getHost();
    UnitApiClient serviceUnitApiClient = new UnitApiClient(
        restTemplateBuilder.basicAuthentication(LOCAL_ADMIN_USERNAME, LOCAL_ADMIN_PASSWORD)
            .build(), userUrl);
    UserApiClient serviceUserApiClient = new UserApiClient(
        restTemplateBuilder.basicAuthentication(LOCAL_ADMIN_USERNAME, LOCAL_ADMIN_PASSWORD)
            .build(), userUrl);

    serviceUnitApiClient.createUnit(CreateUnitDto.builder()
        .code(UNIT_CODE_1)
        .status(UnitStatus.ACTIVE)
        .name(UNIT_NAME_1)
        .searchText(UNIT_1_SEARCH_TEXT)
        .build());

    serviceUnitApiClient.createUnit(CreateUnitDto.builder()
        .code(UNIT_CODE_2)
        .status(UnitStatus.ACTIVE)
        .externalId(UNIT_2_EXTERNAL_ID)
        .name(UNIT_NAME_2)
        .searchText(String.join(" ", UNIT_CODE_2, UNIT_NAME_2, "Bern"))
        .build());

    serviceUserApiClient.createUser(CreateUserDto.builder()
        .code(LOCAL_USER_1_USERNAME)
        .status(UserStatus.ACTIVE)
        .firstName("firstName1")
        .lastName("lastName1")
        .externalId(LOCAL_USER_1_USERNAME)
        .unitCodes(List.of(UNIT_CODE_1))
        .build());

    serviceUserApiClient.createUser(CreateUserDto.builder()
        .code(LOCAL_USER_2_USERNAME)
        .status(UserStatus.ACTIVE)
        .firstName("firstName2")
        .lastName("lastName2")
        .externalId(LOCAL_USER_2_USERNAME)
        .unitCodes(List.of(UNIT_CODE_2))
        .build());

    serviceUserApiClient.createUser(CreateUserDto.builder()
        .code(USER_3_CODE)
        .status(UserStatus.ACTIVE)
        .externalId(USER_3_EXTERNAL_ID)
        .firstName("firstName3")
        .lastName("lastName3")
        .unitCodes(List.of(UNIT_CODE_1, UNIT_CODE_2))
        .build());
  }

  @Test
  void searchUnits() {
    GetUnitsFilters filters = GetUnitsFilters.builder()
        .code(UNIT_CODE_1)
        .name(UNIT_NAME_1)
        .build();
    var responseEntity = getAdminUnitApiClient().getUnits(filters, Pageable.ofSize(20));
    assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
    assertThat(responseEntity.getBody().getContent()).hasSize(1);
    var foundUnit = responseEntity.getBody().getContent().getFirst();
    assertThat(foundUnit.getCode()).isEqualTo(UNIT_CODE_1);
    assertThat(foundUnit.getStatus()).isEqualTo(UnitStatus.ACTIVE);
    assertThat(foundUnit.getName()).isEqualTo(UNIT_NAME_1);

  }

  @Test
  @DisplayName("Search units filtering by user code")
  void searchUnitsFilteringByUserCode() {
    GetUnitsFilters filters = GetUnitsFilters.builder()
        .userCode(USER_3_CODE)
        .build();
    var responseEntity = getAdminUnitApiClient().getUnits(filters, Pageable.ofSize(20));
    assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
    Page<UnitDto> responseEntityBody = responseEntity.getBody();
    assertThat(responseEntityBody).isNotNull();
    var foundUnits = responseEntityBody.getContent();
    assertThat(foundUnits).hasSize(2);
    assertThat(foundUnits).extracting(UnitDto::getCode)
        .containsExactlyInAnyOrder(UNIT_CODE_1, UNIT_CODE_2);
  }

  @Test
  @DisplayName("Search users filtering by searchText")
  void searchUnitsFilteringBySearchText() {
    GetUnitsFilters filters = GetUnitsFilters.builder()
        .searchText("Zürich")
        .build();
    var responseEntity = getAdminUnitApiClient().getUnits(filters, Pageable.ofSize(20));
    assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
    Page<UnitDto> responseEntityBody = responseEntity.getBody();
    assertThat(responseEntityBody).isNotNull();
    var foundUnits = responseEntityBody.getContent();
    assertThat(foundUnits).extracting(UnitDto::getSearchText)
        .containsExactly(UNIT_1_SEARCH_TEXT);
    assertThat(foundUnits).extracting(UnitDto::getCode)
        .containsExactlyInAnyOrder(UNIT_CODE_1);
  }

  @Test
  void searchUsers() {
    GetUsersFilters filters = GetUsersFilters.builder()
        .codes(List.of(USER_3_CODE))
        .build();
    var responseEntity = getAdminUserApiClient().getUsers(filters, Pageable.ofSize(20));
    assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
    assertThat(responseEntity.getBody().getContent()).hasSize(1);
    var foundUser = responseEntity.getBody().getContent().getFirst();
    assertThat(foundUser.getCode()).isEqualTo(USER_3_CODE);
    assertThat(foundUser.getStatus()).isEqualTo(UserStatus.ACTIVE);
    assertThat(foundUser.getUnitCodes()).containsExactlyInAnyOrder(UNIT_CODE_1, UNIT_CODE_2);
    // No unitCode filter, so no unitStatus and roles
    assertThat(foundUser.getRoles()).isNull();
    assertThat(foundUser.getUnitStatus()).isNull();

  }

  @Test
  void searchUsersInUnitNotInZitadel() {
    GetUsersFilters filters = GetUsersFilters.builder()
        .codes(List.of(LOCAL_USER_1_USERNAME))
        .unitCode(UNIT_CODE_1)
        .build();
    var responseEntity = getAdminUserApiClient().getUsers(filters, Pageable.ofSize(20));
    assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
    assertThat(responseEntity.getBody().getContent()).hasSize(1);
    var foundUser = responseEntity.getBody().getContent().getFirst();
    assertThat(foundUser.getCode()).isEqualTo(LOCAL_USER_1_USERNAME);
    assertThat(foundUser.getStatus()).isEqualTo(UserStatus.ACTIVE);
    assertThat(foundUser.getUnitCodes()).contains(UNIT_CODE_1);
    assertThat(foundUser.getRoles()).isNull();
    assertThat(foundUser.getUnitStatus()).isEqualTo(UnitStatus.ACTIVE);

    verifyNoInteractions(zitadelManagementApiClient);
  }

  @Test
  void searchUsersInUnitInZitadel() {

    when(zitadelManagementApiClient.searchUserGrants(eq(UNIT_2_EXTERNAL_ID), anyString()))
        .thenReturn(ResponseEntity.ok(SearchUserGrantsResponseDto.builder()
            .result(List.of(ResultDTO.builder()
                .userId(USER_3_EXTERNAL_ID)
                .roleKeys(List.of("role1", "role2"))
                .build()))
            .build()));

    GetUsersFilters filters = GetUsersFilters.builder()
        .codes(List.of(USER_3_CODE))
        .unitCode(UNIT_CODE_2)
        .build();
    
    var responseEntity = getAdminUserApiClient().getUsers(filters, Pageable.ofSize(20));
    assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
    assertThat(responseEntity.getBody().getContent()).hasSize(1);
    var foundUser = responseEntity.getBody().getContent().getFirst();
    assertThat(foundUser.getCode()).isEqualTo(USER_3_CODE);
    assertThat(foundUser.getStatus()).isEqualTo(UserStatus.ACTIVE);
    assertThat(foundUser.getUnitCodes()).contains(UNIT_CODE_2);
    assertThat(foundUser.getRoles()).isNotNull().contains("role1", "role2");
    assertThat(foundUser.getUnitStatus()).isEqualTo(UnitStatus.ACTIVE);

  }

  @Test
  void searchUsersInUnitInZitadelProblems() {

    when(zitadelManagementApiClient.searchUserGrants(eq(UNIT_2_EXTERNAL_ID), anyString()))
        .thenThrow(new ResourceAccessException("Connection refused"));

    GetUsersFilters filters = GetUsersFilters.builder()
        .codes(List.of(LOCAL_USER_2_USERNAME))
        .unitCode(UNIT_CODE_2)
        .build();

    var responseEntity = getAdminUserApiClient().getUsers(filters, Pageable.ofSize(20));
    assertThat(responseEntity.getStatusCode()).isEqualTo(OK);
    assertThat(responseEntity.getBody().getContent()).hasSize(1);
    var foundUser = responseEntity.getBody().getContent().getFirst();
    assertThat(foundUser.getCode()).isEqualTo(LOCAL_USER_2_USERNAME);
    assertThat(foundUser.getStatus()).isEqualTo(UserStatus.ACTIVE);
    assertThat(foundUser.getUnitCodes()).contains(UNIT_CODE_2);
    assertThat(foundUser.getRoles()).isNull();

  }


}
