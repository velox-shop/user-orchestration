# noinspection SqlNoDataSourceInspectionForFile
USE mysql;
GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;

# create databases
CREATE DATABASE IF NOT EXISTS `slyuserdb`;

# create root user and grant rights
GRANT ALL PRIVILEGES ON *.* TO 'slyusr'@'%';
FLUSH PRIVILEGES;
