package shop.velox.user_orchestration.api.dto;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@FieldNameConstants
public class ZitadelUserDto {

  String id;

  String userName;

  String nickName;

  String unitExternalId;

}
