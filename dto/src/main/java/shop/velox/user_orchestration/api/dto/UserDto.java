package shop.velox.user_orchestration.api.dto;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;
import shop.velox.user.api.dto.address.AddressReferenceDto;
import shop.velox.user.enumerations.UnitStatus;
import shop.velox.user.enumerations.UserStatus;
import shop.velox.user.serialization.UserStatusDeserializer;

@Value
@Builder
@Jacksonized
@FieldNameConstants
@Schema(name = "User")
public class UserDto {

  @NotBlank
  @Schema(
      description = "Unique identifier of the user")
  String code;

  @Schema(description = "Unique identifier of the user on an external system")
  String externalId;

  @Schema(description = "Status of the user")
  @JsonDeserialize(using = UserStatusDeserializer.class)
  UserStatus status;

  @Schema(description = "First name of the user")
  String firstName;

  @Schema(description = "Last name of the user")
  String lastName;

  @Schema(description = "Phone number of the user")
  String phone;

  @Schema(description = "Email address of the user")
  String email;

  @Schema(description = "Preferred language of the user. ISO 639-1 language code", example = "en")
  String preferredLanguage;

  @NotNull
  @Builder.Default
  @Schema(description = "Addresses of the user")
  @Valid
  List<AddressReferenceDto> addresses = new ArrayList<>();

  @NotNull
  @Builder.Default
  @Schema(description = "Codes of Units the user is part of")
  List<String> unitCodes = new ArrayList<>();

  @Schema(description = "The Status of one of Unit the user is part of (which one comes from the controller filter). If null, this info was not loaded.")
  UnitStatus unitStatus;

  @Schema(description = "Roles of the user on Zitadel. If null, this info was not loaded.")
  List<String> roles;

}
