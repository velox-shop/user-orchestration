package shop.velox.user_orchestration.api.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import java.util.List;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.FieldNameConstants;
import lombok.extern.jackson.Jacksonized;

@Value
@Builder
@Jacksonized
@FieldNameConstants
public class CreateUserZitadelDto {

  @Schema(description = "code of the Unit the User will be created into", example = "689000")
  String unitCode;

  @Schema(description = "Roles for the user")
  List<String> roles;

}
